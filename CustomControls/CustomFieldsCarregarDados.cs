﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCA.CustomControls
{
    public partial class CustomFieldsCarregarDados : UserControl
    {
        public CustomFieldsCarregarDados()
        {
            InitializeComponent();
        }

        public string ValorTipoOperacao
        {
            get { return txtTipoOperacao.Text; }
            set { txtTipoOperacao.Text = value; }
        }

        public string ValorQuantidade
        {
            get { return txtQuantidade.Text; }
            set { txtQuantidade.Text = value; }
        }

        public string ValorTicker
        {
            get { return txtTicker.Text; }
            set { txtTicker.Text = value; }
        }

        public string ValorTitulo
        {
            get { return txtTitulo.Text; }
            set { txtTitulo.Text = value; }
        }

        public string ValorTotal
        {
            get { return txtTotal.Text; }
            set { txtTotal.Text = value; }
        }

        public string Valor
        {
            get { return txtValor.Text; }
            set { txtValor.Text = value; }
        }

        public Button btnFechar
        {
            get { return btnExcluir; }
            set { btnExcluir = value; }
        }
    }
}
