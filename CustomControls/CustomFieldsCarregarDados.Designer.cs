﻿
namespace CCA.CustomControls
{
    partial class CustomFieldsCarregarDados
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlDadosClone = new System.Windows.Forms.Panel();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblValor = new System.Windows.Forms.Label();
            this.txtQuantidade = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblQuantidade = new System.Windows.Forms.Label();
            this.txtTicker = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblAtivo = new System.Windows.Forms.Label();
            this.txtTitulo = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.txtTipoOperacao = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblTipoOperacao = new System.Windows.Forms.Label();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.pnlDadosClone.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDadosClone
            // 
            this.pnlDadosClone.Controls.Add(this.btnExcluir);
            this.pnlDadosClone.Controls.Add(this.txtTotal);
            this.pnlDadosClone.Controls.Add(this.panel9);
            this.pnlDadosClone.Controls.Add(this.lblTotal);
            this.pnlDadosClone.Controls.Add(this.txtValor);
            this.pnlDadosClone.Controls.Add(this.panel8);
            this.pnlDadosClone.Controls.Add(this.lblValor);
            this.pnlDadosClone.Controls.Add(this.txtQuantidade);
            this.pnlDadosClone.Controls.Add(this.panel7);
            this.pnlDadosClone.Controls.Add(this.lblQuantidade);
            this.pnlDadosClone.Controls.Add(this.txtTicker);
            this.pnlDadosClone.Controls.Add(this.panel6);
            this.pnlDadosClone.Controls.Add(this.lblAtivo);
            this.pnlDadosClone.Controls.Add(this.txtTitulo);
            this.pnlDadosClone.Controls.Add(this.panel4);
            this.pnlDadosClone.Controls.Add(this.lblTitulo);
            this.pnlDadosClone.Controls.Add(this.txtTipoOperacao);
            this.pnlDadosClone.Controls.Add(this.panel3);
            this.pnlDadosClone.Controls.Add(this.lblTipoOperacao);
            this.pnlDadosClone.Location = new System.Drawing.Point(0, 0);
            this.pnlDadosClone.Name = "pnlDadosClone";
            this.pnlDadosClone.Size = new System.Drawing.Size(924, 48);
            this.pnlDadosClone.TabIndex = 1;
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotal.Enabled = false;
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTotal.ForeColor = System.Drawing.Color.Silver;
            this.txtTotal.Location = new System.Drawing.Point(815, 14);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(80, 16);
            this.txtTotal.TabIndex = 19;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel9.Location = new System.Drawing.Point(815, 24);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(80, 10);
            this.panel9.TabIndex = 18;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblTotal.ForeColor = System.Drawing.Color.White;
            this.lblTotal.Location = new System.Drawing.Point(776, 15);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(37, 15);
            this.lblTotal.TabIndex = 17;
            this.lblTotal.Text = "Total:";
            // 
            // txtValor
            // 
            this.txtValor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.txtValor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtValor.Enabled = false;
            this.txtValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtValor.ForeColor = System.Drawing.Color.Silver;
            this.txtValor.Location = new System.Drawing.Point(702, 14);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(65, 16);
            this.txtValor.TabIndex = 16;
            this.txtValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel8.Location = new System.Drawing.Point(702, 24);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(65, 10);
            this.panel8.TabIndex = 15;
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblValor.ForeColor = System.Drawing.Color.White;
            this.lblValor.Location = new System.Drawing.Point(662, 15);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(38, 15);
            this.lblValor.TabIndex = 14;
            this.lblValor.Text = "Valor:";
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.txtQuantidade.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtQuantidade.Enabled = false;
            this.txtQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtQuantidade.ForeColor = System.Drawing.Color.Silver;
            this.txtQuantidade.Location = new System.Drawing.Point(588, 14);
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.Size = new System.Drawing.Size(65, 16);
            this.txtQuantidade.TabIndex = 13;
            this.txtQuantidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel7.Location = new System.Drawing.Point(588, 24);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(65, 10);
            this.panel7.TabIndex = 12;
            // 
            // lblQuantidade
            // 
            this.lblQuantidade.AutoSize = true;
            this.lblQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblQuantidade.ForeColor = System.Drawing.Color.White;
            this.lblQuantidade.Location = new System.Drawing.Point(512, 15);
            this.lblQuantidade.Name = "lblQuantidade";
            this.lblQuantidade.Size = new System.Drawing.Size(74, 15);
            this.lblQuantidade.TabIndex = 11;
            this.lblQuantidade.Text = "Quantidade:";
            // 
            // txtTicker
            // 
            this.txtTicker.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.txtTicker.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTicker.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTicker.ForeColor = System.Drawing.Color.White;
            this.txtTicker.Location = new System.Drawing.Point(438, 14);
            this.txtTicker.MaxLength = 6;
            this.txtTicker.Name = "txtTicker";
            this.txtTicker.Size = new System.Drawing.Size(65, 16);
            this.txtTicker.TabIndex = 10;
            this.txtTicker.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel6.Location = new System.Drawing.Point(438, 24);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(65, 10);
            this.panel6.TabIndex = 9;
            // 
            // lblAtivo
            // 
            this.lblAtivo.AutoSize = true;
            this.lblAtivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblAtivo.ForeColor = System.Drawing.Color.White;
            this.lblAtivo.Location = new System.Drawing.Point(400, 15);
            this.lblAtivo.Name = "lblAtivo";
            this.lblAtivo.Size = new System.Drawing.Size(35, 15);
            this.lblAtivo.TabIndex = 8;
            this.lblAtivo.Text = "Ativo:";
            // 
            // txtTitulo
            // 
            this.txtTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.txtTitulo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTitulo.Enabled = false;
            this.txtTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTitulo.ForeColor = System.Drawing.Color.Silver;
            this.txtTitulo.Location = new System.Drawing.Point(241, 14);
            this.txtTitulo.Name = "txtTitulo";
            this.txtTitulo.Size = new System.Drawing.Size(150, 16);
            this.txtTitulo.TabIndex = 7;
            this.txtTitulo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel4.Location = new System.Drawing.Point(241, 24);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(150, 10);
            this.panel4.TabIndex = 6;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(199, 15);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(40, 15);
            this.lblTitulo.TabIndex = 5;
            this.lblTitulo.Text = "Titulo:";
            // 
            // txtTipoOperacao
            // 
            this.txtTipoOperacao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.txtTipoOperacao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTipoOperacao.Enabled = false;
            this.txtTipoOperacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTipoOperacao.ForeColor = System.Drawing.Color.Silver;
            this.txtTipoOperacao.Location = new System.Drawing.Point(123, 14);
            this.txtTipoOperacao.Name = "txtTipoOperacao";
            this.txtTipoOperacao.Size = new System.Drawing.Size(67, 16);
            this.txtTipoOperacao.TabIndex = 4;
            this.txtTipoOperacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel3.Location = new System.Drawing.Point(123, 24);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(67, 10);
            this.panel3.TabIndex = 3;
            // 
            // lblTipoOperacao
            // 
            this.lblTipoOperacao.AutoSize = true;
            this.lblTipoOperacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblTipoOperacao.ForeColor = System.Drawing.Color.White;
            this.lblTipoOperacao.Location = new System.Drawing.Point(30, 15);
            this.lblTipoOperacao.Name = "lblTipoOperacao";
            this.lblTipoOperacao.Size = new System.Drawing.Size(91, 15);
            this.lblTipoOperacao.TabIndex = 0;
            this.lblTipoOperacao.Text = "Tipo Operacao:";
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatAppearance.BorderSize = 0;
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnExcluir.Location = new System.Drawing.Point(906, 15);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(15, 20);
            this.btnExcluir.TabIndex = 20;
            this.btnExcluir.Text = "X";
            this.btnExcluir.UseVisualStyleBackColor = true;
            // 
            // CustomFieldsCarregarDados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.Controls.Add(this.pnlDadosClone);
            this.Name = "CustomFieldsCarregarDados";
            this.Size = new System.Drawing.Size(924, 48);
            this.pnlDadosClone.ResumeLayout(false);
            this.pnlDadosClone.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlDadosClone;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.TextBox txtQuantidade;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblQuantidade;
        private System.Windows.Forms.TextBox txtTicker;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblAtivo;
        private System.Windows.Forms.TextBox txtTitulo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.TextBox txtTipoOperacao;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblTipoOperacao;
        private System.Windows.Forms.Button btnExcluir;
    }
}
