﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCA.Util
{
    public enum EnumBotoesLaterais
    {
        btnAnaliseDados,

        btnDashboard,

        btnImportarDados,

        btnImportarOperacoes,

        btnImportarRendimentos
    }
}
