﻿using Dados.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Dados.Controllers
{
    public class DescricaoAtivosController
    {
        public int InserirRegistro(DESCRICAO_ATIVOS DescricaoAtivos)
        {
            try
            {
                DESCRICAO_ATIVOS instDescricaoAtivos = new DESCRICAO_ATIVOS();
                Database.CCAEntities db = new CCAEntities();
                db.DESCRICAO_ATIVOS.Add(DescricaoAtivos);
                db.SaveChanges();
                return instDescricaoAtivos.ID;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao Inserir DescricaoAtivos: " + ex.Message);
            }
        }

        public List<DESCRICAO_ATIVOS> RecuperarRegistro(int idAtivo = 0, string DescricaoAtivos = null)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();

                if (idAtivo != 0 && !string.IsNullOrEmpty(DescricaoAtivos))
                    return db.DESCRICAO_ATIVOS.Include(a => a.ATIVOS).Where(x => x.ID_ATIVO == idAtivo && x.DESCRICAO_ATIVO == DescricaoAtivos).ToList();
                else if (idAtivo != 0)
                    return db.DESCRICAO_ATIVOS.Include(a => a.ATIVOS).Where(x => x.ID_ATIVO == idAtivo).ToList();
                else if (!string.IsNullOrEmpty(DescricaoAtivos))
                    return db.DESCRICAO_ATIVOS.Include(a => a.ATIVOS).Where(x => x.DESCRICAO_ATIVO == DescricaoAtivos).ToList();
                else
                    return db.DESCRICAO_ATIVOS.Include(a => a.ATIVOS).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao recuperar dados: " + ex.Message);
            }
        }

    }
}
