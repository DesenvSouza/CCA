﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Database;

namespace Dados.Controllers
{
    public class DadosBrutos
    {
        public void InserirRegistro(DADOS_BRUTOS DadosBrutos)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                db.DADOS_BRUTOS.Add(DadosBrutos);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao Inserir DadosBrutos: " + ex.Message);
            }
        }

        public List<DADOS_BRUTOS> RecuperarRegistros(int IdLote)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                return db.DADOS_BRUTOS.Where(p => p.ID_LOTE == IdLote).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao Inserir DadosBrutos: " + ex.Message);
            }
        }
    }
}
