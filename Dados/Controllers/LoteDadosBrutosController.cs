﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Database;

namespace Dados.Controllers
{
    public class LoteDadosBrutosController
    {
        public int InserirRegistro()
        {
            try
            {
                LOTE_DADOS_BRUTOS instLoteDadosBrutos = new LOTE_DADOS_BRUTOS() { DATA_IMPORTACAO = DateTime.Now };
                Database.CCAEntities db = new CCAEntities();
                db.LOTE_DADOS_BRUTOS.Add(instLoteDadosBrutos);
                db.SaveChanges();
                return instLoteDadosBrutos.ID;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao Inserir DadosBrutos: " + ex.Message);
            }
        }

        public LOTE_DADOS_BRUTOS RecuperarUltimoLote()
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();

                LOTE_DADOS_BRUTOS MaiorLote = db.LOTE_DADOS_BRUTOS.Where(p => p.DATA_IMPORTACAO == db.LOTE_DADOS_BRUTOS.Max(r => r.DATA_IMPORTACAO)).FirstOrDefault();

                return MaiorLote;
            }

            catch (Exception ex)
            {
                throw new Exception("Erro Ao Inserir DadosBrutos: " + ex.Message);
            }
        }
    }
}
