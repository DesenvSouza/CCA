﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Database;

namespace Dados.Controllers
{
    public class OperacoesController
    {
        public void InserirRegistro(OPERACOES Operacao)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                db.OPERACOES.Add(Operacao);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao Inserir Registro: " + ex.Message);
            }
        }
    }
}
