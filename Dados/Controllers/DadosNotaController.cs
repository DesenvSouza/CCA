﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Database;

namespace Dados.Controllers
{
    public class DadosNotaController
    {
        public void InserirRegistro(DADOS_NOTA Nota)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                db.DADOS_NOTA.Add(Nota);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao Inserir Ativo: " + ex.Message);
            }
        }

        public List<DADOS_NOTA> RecuperarRegistros(int NumeroNota = 0)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                if (NumeroNota != 0)
                    return db.DADOS_NOTA.Where(p => p.NUMERO_NOTA == NumeroNota).ToList();
                else
                    return db.DADOS_NOTA.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao recuperar Ativo: " + ex.Message);
            }
        }

        public DADOS_NOTA RecuperarUltimoPregaoImportado()
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                return db.DADOS_NOTA.Where(p => p.ID == db.DADOS_NOTA.Max(r => r.ID)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao recuperar Ativo: " + ex.Message);
            }
        }
    }
}
