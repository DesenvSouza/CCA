﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Database;

namespace Dados.Controllers
{
    public class TipoOperacaoController
    {

        public List<TIPO_OPERACAO> RecuperarRegistros(string Descricao = null)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                if (!string.IsNullOrEmpty(Descricao))
                    return db.TIPO_OPERACAO.Where(p => p.DESCRICAO == Descricao).ToList();
                else
                    return db.TIPO_OPERACAO.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao recuperar Ativo: " + ex.Message);
            }
        }
    }
}
