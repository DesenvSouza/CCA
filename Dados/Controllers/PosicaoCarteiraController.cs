﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Database;
using System.Data.Entity;


namespace Dados.Controllers
{
    public class PosicaoCarteiraController
    {
        public List<POSICAO_CARTEIRA> RecuperarOperacoesComResultado()
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                return db.POSICAO_CARTEIRA.Where(p => p.RESULTADO_OPERACAO != 0).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao recuperar Ativo: " + ex.Message);
            }
        }

        public List<POSICAO_CARTEIRA> RecuperarOperacoesEmAberto()
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                //return db.POSICAO_CARTEIRA.Where(p => p.PRECO_TOTAL_POSICAO != 0).ToList();
                return db.POSICAO_CARTEIRA.Include(a => a.ATIVOS).Include(s => s.DESCRICAO_STATUS).Where(p => p.PRECO_TOTAL_POSICAO != 0).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao recuperar Ativo: " + ex.Message);
            }
        }
    }
}
