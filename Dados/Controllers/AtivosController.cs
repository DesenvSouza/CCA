﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Database;
using Dados.Util;

namespace Dados.Controllers
{
    public class AtivosController
    {
        public int InserirRegistro(ATIVOS Ativo)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                db.ATIVOS.Add(Ativo);
                db.SaveChanges();
                return Ativo.ID;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao Inserir Ativo: " + ex.Message);
            }
        }

        public void AtualizarRegistro(ATIVOS Ativo)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();

                var AtivoResult = db.ATIVOS.SingleOrDefault(x => x.CODIGO == Ativo.CODIGO);
                if(AtivoResult != null)
                {
                    AtivoResult.DATA_ALTERACAO = DateTime.Now;
                    AtivoResult.DESCRICAO_ATIVO = Ativo.DESCRICAO_ATIVO;
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao atualizar o Ativo: " + Ativo.CODIGO + " - " + ex.Message);
            }
        }

        public List<ATIVOS> RecuperarRegistros(string DescricaoAtivo = null, 
                                                    string Ticker = null, 
                                                        EnumTipoOperador.TipoOperador TipoOperador = EnumTipoOperador.TipoOperador.And)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();

                if(!string.IsNullOrEmpty(DescricaoAtivo) && !string.IsNullOrEmpty(Ticker))
                {
                    switch (TipoOperador)
                    {
                        case EnumTipoOperador.TipoOperador.And:
                            return db.ATIVOS.Where(p => p.DESCRICAO_ATIVO == DescricaoAtivo && p.CODIGO == Ticker).ToList();
                        case EnumTipoOperador.TipoOperador.Or:
                            return db.ATIVOS.Where(p => p.DESCRICAO_ATIVO == DescricaoAtivo || p.CODIGO == Ticker).ToList();
                        default: return null;
                    }
                }
                else if (!string.IsNullOrEmpty(DescricaoAtivo))
                    return db.ATIVOS.Where(p => p.DESCRICAO_ATIVO == DescricaoAtivo).ToList();
                else if (!string.IsNullOrEmpty(Ticker))
                    return db.ATIVOS.Where(p => p.CODIGO == Ticker).ToList();
                else
                    return db.ATIVOS.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao recuperar Ativo: " + ex.Message);
            }
        }
    }
}

