﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Database;
namespace Dados.Controllers
{
    public class ParametrosFiltros
    {
        public List<PARAMETROS_FILTRO> RecuperarRegistros(int NumeroNota = 0)
        {
            try
            {
                Database.CCAEntities db = new CCAEntities();
                return db.PARAMETROS_FILTRO.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Ao recuperar Ativo: " + ex.Message);
            }
        }

    }
}
