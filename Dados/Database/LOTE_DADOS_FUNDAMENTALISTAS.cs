//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dados.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class LOTE_DADOS_FUNDAMENTALISTAS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LOTE_DADOS_FUNDAMENTALISTAS()
        {
            this.DADOS_FUNDAMENTALISTAS = new HashSet<DADOS_FUNDAMENTALISTAS>();
        }
    
        public int ID { get; set; }
        public System.DateTime DATA_LOTE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DADOS_FUNDAMENTALISTAS> DADOS_FUNDAMENTALISTAS { get; set; }
    }
}
