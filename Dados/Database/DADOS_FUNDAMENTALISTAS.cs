//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dados.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class DADOS_FUNDAMENTALISTAS
    {
        public int ID { get; set; }
        public int ID_LOTE { get; set; }
        public int ID_ATIVO { get; set; }
        public Nullable<decimal> VALOR_ACAO { get; set; }
        public Nullable<System.DateTime> DATA_COTACAO { get; set; }
        public Nullable<int> PAPEIS_CIRCULACAO { get; set; }
        public Nullable<decimal> VALOR_MERCADO { get; set; }
        public Nullable<decimal> VALOR_DA_FIRMA { get; set; }
        public Nullable<decimal> ATIVO { get; set; }
        public Nullable<decimal> DISPONIBILIDADES { get; set; }
        public Nullable<decimal> ATIVO_CIRCULANTE { get; set; }
        public Nullable<decimal> CARTEIRA_DE_CREDITO { get; set; }
        public Nullable<decimal> DIVIDA_BRUTA { get; set; }
        public Nullable<decimal> DIVIDA_LIQUIDA { get; set; }
        public Nullable<decimal> PATRIMONIO_LIQUIDO { get; set; }
        public Nullable<decimal> DEPOSITOS { get; set; }
        public Nullable<decimal> RECEITA_LIQUIDA { get; set; }
        public Nullable<decimal> EBIT { get; set; }
        public Nullable<decimal> LUCRO_LIQUIDO { get; set; }
        public Nullable<decimal> RECEITA_LIQUIDA_12M { get; set; }
        public Nullable<decimal> EBIT_12M { get; set; }
        public Nullable<decimal> LUCRO_LIQUIDO_12M { get; set; }
        public Nullable<decimal> RESULTADO_BRUTO_INTERMEDIACOES_FINANCEIRAS { get; set; }
        public Nullable<decimal> RESULTADO_BRUTO_INTERMEDIACOES_FINANCEIRAS_12M { get; set; }
        public Nullable<decimal> RECEITAS_PRESTACAO_DE_SERVICOS { get; set; }
        public Nullable<decimal> RECEITAS_PRESTACAO_DE_SERVICOS_12M { get; set; }
        public Nullable<decimal> LPA { get; set; }
        public Nullable<decimal> PL { get; set; }
        public Nullable<decimal> VPA { get; set; }
        public Nullable<decimal> P_VP { get; set; }
        public Nullable<decimal> P_EBIT { get; set; }
        public Nullable<decimal> DIVIDEND_YIELD { get; set; }
        public Nullable<decimal> MARGE_BRUTA { get; set; }
        public Nullable<decimal> MARGEM_BRUTA_12M { get; set; }
        public Nullable<decimal> PSR { get; set; }
        public Nullable<decimal> MARGEM_EBIT { get; set; }
        public Nullable<decimal> MARGEM_EBIT_12M { get; set; }
        public Nullable<decimal> P_ATIVOS { get; set; }
        public Nullable<decimal> MARGEM_LIQUIDA { get; set; }
        public Nullable<decimal> MARGEM_LIQUIDA_12M { get; set; }
        public Nullable<decimal> P_CAPITAL_GIRO { get; set; }
        public Nullable<decimal> EBIT_ATIVO { get; set; }
        public Nullable<decimal> ROIC { get; set; }
        public Nullable<decimal> ROIC_12M { get; set; }
        public Nullable<decimal> ROE { get; set; }
        public Nullable<decimal> EV_EBIT { get; set; }
        public Nullable<decimal> EV_EBIT_12M { get; set; }
        public Nullable<decimal> LIQUIDEZ_CORRENTE { get; set; }
        public Nullable<decimal> DIVIDA_BRUTA_PATRIMONIAL { get; set; }
        public Nullable<decimal> GIRO_ATIVOS { get; set; }
    
        public virtual ATIVOS ATIVOS { get; set; }
        public virtual LOTE_DADOS_FUNDAMENTALISTAS LOTE_DADOS_FUNDAMENTALISTAS { get; set; }
    }
}
