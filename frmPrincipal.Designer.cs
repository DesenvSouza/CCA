﻿
namespace CCA
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlConteudo = new System.Windows.Forms.Panel();
            this.pnlBarraTitulo = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.btnAjuda = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.pnlMenusLaterais = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btnImportarRendimentos = new System.Windows.Forms.Button();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btnImportarOperacoes = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnImportarDados = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnAnaliseDados = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnDashboard = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.pnlBarraTitulo.SuspendLayout();
            this.pnlMenusLaterais.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(44)))), ((int)(((byte)(68)))));
            this.panel1.Controls.Add(this.pnlConteudo);
            this.panel1.Controls.Add(this.pnlBarraTitulo);
            this.panel1.Controls.Add(this.pnlMenusLaterais);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1173, 698);
            this.panel1.TabIndex = 0;
            // 
            // pnlConteudo
            // 
            this.pnlConteudo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlConteudo.Location = new System.Drawing.Point(193, 40);
            this.pnlConteudo.Name = "pnlConteudo";
            this.pnlConteudo.Size = new System.Drawing.Size(980, 658);
            this.pnlConteudo.TabIndex = 2;
            // 
            // pnlBarraTitulo
            // 
            this.pnlBarraTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.pnlBarraTitulo.Controls.Add(this.button3);
            this.pnlBarraTitulo.Controls.Add(this.btnAjuda);
            this.pnlBarraTitulo.Controls.Add(this.btnFechar);
            this.pnlBarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBarraTitulo.Location = new System.Drawing.Point(193, 0);
            this.pnlBarraTitulo.Name = "pnlBarraTitulo";
            this.pnlBarraTitulo.Size = new System.Drawing.Size(980, 40);
            this.pnlBarraTitulo.TabIndex = 1;
            this.pnlBarraTitulo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlBarraTitulo_MouseMove);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.button3.Dock = System.Windows.Forms.DockStyle.Right;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(860, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(40, 40);
            this.button3.TabIndex = 2;
            this.button3.Text = "-";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            // 
            // btnAjuda
            // 
            this.btnAjuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(160)))), ((int)(((byte)(220)))));
            this.btnAjuda.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAjuda.FlatAppearance.BorderSize = 0;
            this.btnAjuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjuda.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjuda.ForeColor = System.Drawing.Color.White;
            this.btnAjuda.Image = global::CCA.Properties.Resources.informacoes;
            this.btnAjuda.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAjuda.Location = new System.Drawing.Point(900, 0);
            this.btnAjuda.Name = "btnAjuda";
            this.btnAjuda.Size = new System.Drawing.Size(40, 40);
            this.btnAjuda.TabIndex = 1;
            this.btnAjuda.UseVisualStyleBackColor = false;
            this.btnAjuda.Click += new System.EventHandler(this.btnAjuda_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(90)))), ((int)(((byte)(74)))));
            this.btnFechar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.ForeColor = System.Drawing.Color.White;
            this.btnFechar.Image = global::CCA.Properties.Resources.erro;
            this.btnFechar.Location = new System.Drawing.Point(940, 0);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(40, 40);
            this.btnFechar.TabIndex = 0;
            this.btnFechar.UseVisualStyleBackColor = false;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // pnlMenusLaterais
            // 
            this.pnlMenusLaterais.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.pnlMenusLaterais.Controls.Add(this.panel16);
            this.pnlMenusLaterais.Controls.Add(this.panel15);
            this.pnlMenusLaterais.Controls.Add(this.panel13);
            this.pnlMenusLaterais.Controls.Add(this.panel12);
            this.pnlMenusLaterais.Controls.Add(this.panel10);
            this.pnlMenusLaterais.Controls.Add(this.panel3);
            this.pnlMenusLaterais.Controls.Add(this.panel8);
            this.pnlMenusLaterais.Controls.Add(this.panel7);
            this.pnlMenusLaterais.Controls.Add(this.panel5);
            this.pnlMenusLaterais.Controls.Add(this.panel4);
            this.pnlMenusLaterais.Controls.Add(this.panel2);
            this.pnlMenusLaterais.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenusLaterais.Location = new System.Drawing.Point(0, 0);
            this.pnlMenusLaterais.Name = "pnlMenusLaterais";
            this.pnlMenusLaterais.Size = new System.Drawing.Size(193, 698);
            this.pnlMenusLaterais.TabIndex = 0;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.btnImportarRendimentos);
            this.panel16.Controls.Add(this.panel17);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 395);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(193, 60);
            this.panel16.TabIndex = 10;
            // 
            // btnImportarRendimentos
            // 
            this.btnImportarRendimentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnImportarRendimentos.FlatAppearance.BorderSize = 0;
            this.btnImportarRendimentos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImportarRendimentos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportarRendimentos.Location = new System.Drawing.Point(5, 0);
            this.btnImportarRendimentos.Name = "btnImportarRendimentos";
            this.btnImportarRendimentos.Size = new System.Drawing.Size(188, 60);
            this.btnImportarRendimentos.TabIndex = 10;
            this.btnImportarRendimentos.Text = "Importar Rendimentos";
            this.btnImportarRendimentos.UseVisualStyleBackColor = true;
            this.btnImportarRendimentos.Click += new System.EventHandler(this.btnImportarRendimentos_Click);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(5, 60);
            this.panel17.TabIndex = 9;
            // 
            // panel15
            // 
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 391);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(193, 4);
            this.panel15.TabIndex = 9;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.btnImportarOperacoes);
            this.panel13.Controls.Add(this.panel14);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 331);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(193, 60);
            this.panel13.TabIndex = 8;
            // 
            // btnImportarOperacoes
            // 
            this.btnImportarOperacoes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnImportarOperacoes.FlatAppearance.BorderSize = 0;
            this.btnImportarOperacoes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImportarOperacoes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportarOperacoes.Location = new System.Drawing.Point(5, 0);
            this.btnImportarOperacoes.Name = "btnImportarOperacoes";
            this.btnImportarOperacoes.Size = new System.Drawing.Size(188, 60);
            this.btnImportarOperacoes.TabIndex = 9;
            this.btnImportarOperacoes.Text = "Importar Operações";
            this.btnImportarOperacoes.UseVisualStyleBackColor = true;
            this.btnImportarOperacoes.Click += new System.EventHandler(this.btnImportarOperacoes_Click);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(5, 60);
            this.panel14.TabIndex = 8;
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 327);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(193, 4);
            this.panel12.TabIndex = 7;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btnImportarDados);
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 267);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(193, 60);
            this.panel10.TabIndex = 6;
            // 
            // btnImportarDados
            // 
            this.btnImportarDados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnImportarDados.FlatAppearance.BorderSize = 0;
            this.btnImportarDados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImportarDados.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportarDados.Location = new System.Drawing.Point(5, 0);
            this.btnImportarDados.Name = "btnImportarDados";
            this.btnImportarDados.Size = new System.Drawing.Size(188, 60);
            this.btnImportarDados.TabIndex = 7;
            this.btnImportarDados.Text = "Importar Dados";
            this.btnImportarDados.UseVisualStyleBackColor = true;
            this.btnImportarDados.Click += new System.EventHandler(this.btnImportarDados_Click);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(5, 60);
            this.panel11.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 263);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(193, 4);
            this.panel3.TabIndex = 5;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnAnaliseDados);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 203);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(193, 60);
            this.panel8.TabIndex = 4;
            // 
            // btnAnaliseDados
            // 
            this.btnAnaliseDados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAnaliseDados.FlatAppearance.BorderSize = 0;
            this.btnAnaliseDados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnaliseDados.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnaliseDados.Location = new System.Drawing.Point(5, 0);
            this.btnAnaliseDados.Name = "btnAnaliseDados";
            this.btnAnaliseDados.Size = new System.Drawing.Size(188, 60);
            this.btnAnaliseDados.TabIndex = 5;
            this.btnAnaliseDados.Text = "Análise de Dados";
            this.btnAnaliseDados.UseVisualStyleBackColor = true;
            this.btnAnaliseDados.Click += new System.EventHandler(this.btnAnaliseDados_Click);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(5, 60);
            this.panel9.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 199);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(193, 4);
            this.panel7.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnDashboard);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 139);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(193, 60);
            this.panel5.TabIndex = 2;
            // 
            // btnDashboard
            // 
            this.btnDashboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDashboard.FlatAppearance.BorderSize = 0;
            this.btnDashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDashboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDashboard.Location = new System.Drawing.Point(5, 0);
            this.btnDashboard.Name = "btnDashboard";
            this.btnDashboard.Size = new System.Drawing.Size(188, 60);
            this.btnDashboard.TabIndex = 3;
            this.btnDashboard.Text = "Dashboard";
            this.btnDashboard.UseVisualStyleBackColor = true;
            this.btnDashboard.Click += new System.EventHandler(this.btnDashboard_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(5, 60);
            this.panel6.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 128);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(193, 11);
            this.panel4.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(193, 128);
            this.panel2.TabIndex = 0;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.ClientSize = new System.Drawing.Size(1175, 700);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPrincipal";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "";
            this.Text = "Principal";
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.panel1.ResumeLayout(false);
            this.pnlBarraTitulo.ResumeLayout(false);
            this.pnlMenusLaterais.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlMenusLaterais;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Panel pnlBarraTitulo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnDashboard;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnAnaliseDados;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnAjuda;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel pnlConteudo;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnImportarDados;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btnImportarOperacoes;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button btnImportarRendimentos;
        private System.Windows.Forms.Panel panel17;
    }
}

