﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Business.Controllers;
using Business.Entidades;


namespace CCA.MenuLateral
{
    public partial class frmDashboard : Form
    {
        public frmDashboard()
        {
            InitializeComponent();
        }

        private void frmDashboard_Load(object sender, EventArgs e)
        {
            try
            {
                decimal dcmTotalAtual = this.PreencherPosicaoCarteira();
                this.ExibirValoresCalculados(dcmTotalAtual);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao Carregar os dados. " + ex.Message);
            }
        }

        private void ExibirValoresCalculados(decimal TotalAtual)
        {
            try
            {
                CtrlPosicaoCarteira instCtrlPosicaoCarteira = new CtrlPosicaoCarteira();

                var listaOperacoesEmAberto = instCtrlPosicaoCarteira.RecuperarOperacoesEmAberto();

                decimal dcmPosicaoInicial = listaOperacoesEmAberto.Sum(x => x.PRECO_TOTAL_POSICAO);
                decimal dcmResultadoConsolidado = instCtrlPosicaoCarteira.RecuperarOperacoesComResultado().Sum(x => x.RESULTADO_OPERACAO);

                this.lblPosicaoInicial.Text = dcmPosicaoInicial.ToString("C2");
                this.lblResultadoConsolidado.Text = dcmResultadoConsolidado.ToString("C2");
                this.lblPosicaoAtual.Text = (dcmPosicaoInicial + TotalAtual).ToString("C2");

                if (dcmResultadoConsolidado > 0)
                    this.lblResultadoConsolidado.ForeColor = Color.LimeGreen;
                else if (dcmResultadoConsolidado < 0)
                    this.lblResultadoConsolidado.ForeColor = Color.Red;
                else
                    this.lblResultadoConsolidado.ForeColor = Color.White;

                if (TotalAtual > 0)
                    this.lblResultadoParcial.ForeColor = Color.LimeGreen;
                else if (TotalAtual < 0)
                    this.lblResultadoParcial.ForeColor = Color.Red;
                else
                    this.lblResultadoParcial.ForeColor = Color.White;

                this.lblResultadoParcial.Text = TotalAtual.ToString("C2");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao Calcular valores atuais. " + ex.Message);
            }
        }

        private decimal PreencherPosicaoCarteira()
        {
            try
            {
                CtrlPosicaoCarteira instPosicaoCarteira = new CtrlPosicaoCarteira();
                var lstListaOperacoes = instPosicaoCarteira.RecuperarOperacoesEmAberto().OrderBy(X => X.ATIVOS.CODIGO);
                decimal dcmTotalPosicaoAtual = 0;


                dgvDadosPosicaoAtual.Columns.Clear();
                dgvDadosPosicaoAtual.Rows.Clear();

                DataSet dtsDados = new DataSet();
                dtsDados.Tables.Add("PosicaoAtual");

                dtsDados.Tables[0].Columns.Add("Ticker").DataType = System.Type.GetType("System.String");
                dtsDados.Tables[0].Columns.Add("Quantidade").DataType = System.Type.GetType("System.Int32");
                dtsDados.Tables[0].Columns.Add("Preço Médio").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Preço Atual").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Valor de Custo").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Valor Atual").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Lucro/Prejuízo").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("% Lucro/Prejuízo").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Ação Recomendada").DataType = System.Type.GetType("System.String");

                foreach (var Dados in lstListaOperacoes)
                {
                    CtrlDadosBrutos instCtrlDadosBrutos = new CtrlDadosBrutos();
                    var lstDadosCotacao = instCtrlDadosBrutos.LerDadosBrutos(Dados.ATIVOS.CODIGO);
                    var lstDadosTratados = instCtrlDadosBrutos.TratarDados(Dados.ATIVOS.CODIGO);

                    decimal dcmValorAtual = 0;
                    decimal dcmValorTotalAtual = 0;

                    if (lstDadosCotacao.Count > 0)
                    {
                        dcmValorAtual = lstDadosCotacao[0].PRECO.Value;
                        dcmValorTotalAtual = (Dados.QUANTIDADE_ABERTA * lstDadosCotacao[0].PRECO.Value);
                    }

                    DataRow dtrLinha = dtsDados.Tables[0].NewRow();
                    dtrLinha[0] = Dados.ATIVOS.CODIGO;
                    dtrLinha[1] = Dados.QUANTIDADE_ABERTA;
                    dtrLinha[2] = Dados.PRECO_MEDIO_ATIVO;
                    dtrLinha[3] = dcmValorAtual;
                    dtrLinha[4] = Dados.PRECO_TOTAL_POSICAO;
                    dtrLinha[5] = dcmValorTotalAtual;
                    dtrLinha[6] = dcmValorTotalAtual - Dados.PRECO_TOTAL_POSICAO;
                    dtrLinha[7] = ((dcmValorTotalAtual - Dados.PRECO_TOTAL_POSICAO) / Dados.PRECO_TOTAL_POSICAO) * 100;

                    dcmTotalPosicaoAtual += (dcmValorTotalAtual - Dados.PRECO_TOTAL_POSICAO);
                    if (lstDadosTratados.Count <= 0)
                    {
                        if (lstDadosCotacao.Count > 0)
                            dtrLinha[8] = "V. FORTE";
                        else
                            dtrLinha[8] = "ATIVO NÃO ENCONTRADO";
                    }
                    else
                    {
                        var dadosAtivo = lstDadosTratados[0];

                        if (dadosAtivo.EarningYield < 10)
                            dtrLinha[8] = "VENDA";
                        else if (dadosAtivo.EarningYield > 20)
                            dtrLinha[8] = "C. FORTE";
                        else if (dadosAtivo.EarningYield > 10)
                            dtrLinha[8] = "COMPRA";
                        else
                            dtrLinha[8] = "N/A";
                    }

                    dtsDados.Tables[0].Rows.Add(dtrLinha);

                    dgvDadosPosicaoAtual.DataSource = dtsDados.Tables[0];

                    dgvDadosPosicaoAtual.Columns[2].DefaultCellStyle.Format = "c2";
                    dgvDadosPosicaoAtual.Columns[3].DefaultCellStyle.Format = "c2";
                    dgvDadosPosicaoAtual.Columns[4].DefaultCellStyle.Format = "c2";
                    dgvDadosPosicaoAtual.Columns[5].DefaultCellStyle.Format = "c2";
                    dgvDadosPosicaoAtual.Columns[6].DefaultCellStyle.Format = "c2";

                    dgvDadosPosicaoAtual.Columns[7].DefaultCellStyle.Format = "0.00\\%";

                    dgvDadosPosicaoAtual.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosPosicaoAtual.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosPosicaoAtual.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosPosicaoAtual.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosPosicaoAtual.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosPosicaoAtual.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosPosicaoAtual.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosPosicaoAtual.Columns[7].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosPosicaoAtual.Columns[8].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                return dcmTotalPosicaoAtual;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        private void dgvDadosPosicaoAtual_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                var dgv = sender as DataGridView;
                if (e.RowIndex < 0 || e.RowIndex == dgv.NewRowIndex) return;

                // Formata a coluna de Lucro/Prejuízo
                var cell = dgv[6, e.RowIndex];
                if (cell.Value == null || cell.Value == DBNull.Value) return;

                decimal dcmValorDecimal = (decimal)cell.Value;
                cell.Style.ForeColor = dcmValorDecimal == 0 ? Color.White : dcmValorDecimal < 0 ? Color.Red : Color.LimeGreen;

                // Formata a coluna de Percentual de Lucro/Prejuízo
                cell = dgv[7, e.RowIndex];
                if (cell.Value == null || cell.Value == DBNull.Value) return;

                dcmValorDecimal = (decimal)cell.Value;
                cell.Style.ForeColor = dcmValorDecimal == 0 ? Color.White : dcmValorDecimal < 0 ? Color.Red : Color.LimeGreen;

                cell = dgv[8, e.RowIndex];
                if (cell.Value == null || cell.Value == DBNull.Value) return;

                string strValorString = cell.Value.ToString();

                if (strValorString == "VENDA")
                    cell.Style.ForeColor = Color.DarkRed;
                else if (strValorString == "V. FORTE")
                    cell.Style.ForeColor = Color.Red;
                else if (strValorString == "COMPRA")
                    cell.Style.ForeColor = Color.ForestGreen;
                else if (strValorString == "C. FORTE")
                    cell.Style.ForeColor = Color.LimeGreen;
                else if (strValorString == "ATIVO NÃO ENCONTRADO")
                    cell.Style.ForeColor = Color.Yellow;
                else
                    cell.Style.ForeColor = Color.White;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao formatar valores " + ex.Message);
            }
        }
    }
}
