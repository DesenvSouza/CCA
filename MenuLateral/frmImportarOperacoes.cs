﻿using Business.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Business.Entidades;
using CCA.CustomControls;

namespace CCA.MenuLateral
{
    public partial class frmImportarOperacoes : Form
    {
        #region DECLARAÇÕES
        private const string DiretorioInicial = "c:\\";
        private const string ExtensaoArquivo = "pdf files (*.pdf)|*.pdf";
        static List<string> lstListaNotas = new List<string>();

        CtrlLerArquivoPdf instLerArquivoPdf;
        CtrlDadosNota instDadosNota;
        #endregion

        public frmImportarOperacoes()
        {
            InitializeComponent();
        }

        private void frmImportarOperacoes_Load(object sender, EventArgs e)
        {
            try
            {
                this.instLerArquivoPdf = new CtrlLerArquivoPdf();
                this.instDadosNota = new CtrlDadosNota();
                this.AtualizarDataUltimoPregao();
            }
            catch
            {
                throw new Exception("Erro ao Iniciar o formulário!");
            }
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            try
            {
                lstListaNotas = new List<string>();
                lstListaNotas = this.IniciarOpenFile();

                if (lstListaNotas.Count > 0)
                {
                    ImportarDados();
                }
                else
                    MessageBox.Show("Nenhum arquivo selecionado.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImportarDados(string txtCaminho = null)
        {
            EntDadosPdf instDadosPdf = new EntDadosPdf();
            if (string.IsNullOrEmpty(txtCaminho))
            {
                instDadosPdf = this.instLerArquivoPdf.LerArquivoOperacoesPdf(lstListaNotas[0]);
                this.txtCaminho.Text = lstListaNotas[0];
            }
            else
            {
                int intIndex = lstListaNotas.IndexOf(txtCaminho) + 1;

                if (intIndex <= (lstListaNotas.Count - 1))
                {
                    string strProximoArquivo = lstListaNotas[intIndex];
                    instDadosPdf = this.instLerArquivoPdf.LerArquivoOperacoesPdf(strProximoArquivo);
                    this.txtCaminho.Text = strProximoArquivo;
                }
                else
                    return;

            }

            var lstNotas = this.instDadosNota.ListarNotas(instDadosPdf.NumeroNota);

            if (lstNotas.Count == 0)
            {
                MontarDadosTela(instDadosPdf);
            }
            else
            {
                //ImportarDados(this.txtCaminho.Text);

                this.pnlListaDados.Controls.Clear();
                MessageBox.Show("Não é possível importar a nota de número " + instDadosPdf.NumeroNota +
                   " pois a mesma já foi importada para o sistema na data " + lstNotas[0].DATA_IMPORTACAO.ToShortDateString(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<string> IniciarOpenFile()
        {
            try
            {

                List<string> lstNotasSelecionadas = new List<string>();
                using (OpenFileDialog ofdSelecionarArquivo = new OpenFileDialog())
                {

                    //ofdSelecionarArquivo.InitialDirectory = DiretorioInicial;
                    ofdSelecionarArquivo.Filter = ExtensaoArquivo;
                    ofdSelecionarArquivo.FilterIndex = 0;
                    ofdSelecionarArquivo.RestoreDirectory = true;
                    ofdSelecionarArquivo.Multiselect = true;

                    if (ofdSelecionarArquivo.ShowDialog() == DialogResult.OK)
                    {
                        this.txtCaminho.Text = ofdSelecionarArquivo.FileName;
                        lstNotasSelecionadas = ofdSelecionarArquivo.FileNames.ToList();
                    }
                    else
                    {
                        return new List<string>();
                    }
                }
                return lstNotasSelecionadas;
            }
            catch (Exception ex)
            {
                return new List<string>();
            }
        }

        private void MontarDadosTela(EntDadosPdf DadosPdf)
        {
            this.pnlListaDados.Controls.Clear();

            if (!string.IsNullOrEmpty(DadosPdf.NumeroNota.ToString()) &&
                DadosPdf.DataPregao != DateTime.MinValue)
            {
                this.txtDataPregao.Text = DadosPdf.DataPregao.ToShortDateString();
                this.txtNumeroNota.Text = DadosPdf.NumeroNota.ToString();
                this.pnlDadosNota.Visible = true;
                CtrlDescricaoAtivos instCtrlDescricaoAtivos = new CtrlDescricaoAtivos();
                DadosPdf.ListaOperacoes.Reverse();
                int contTabIndex = DadosPdf.ListaOperacoes.Count;
                foreach (EntDadosOperacoesPdf Operacao in DadosPdf.ListaOperacoes)
                {
                    CustomFieldsCarregarDados instCustom = new CustomFieldsCarregarDados();
                    var lstAtivos = instCtrlDescricaoAtivos.RecuperarRegistro(0, Operacao.EspecificacaoTitulo.ToString());
                    instCustom.ValorTipoOperacao = Operacao.TipoOperacao.ToString();
                    instCustom.Valor = Operacao.Preco.ToString("C2");
                    if (lstAtivos.Count > 0)
                    {
                        instCustom.ValorTicker = lstAtivos[0].ATIVOS.CODIGO;
                    }
                    instCustom.ValorTotal = Operacao.ValorOperacao.ToString("C2");
                    instCustom.ValorQuantidade = Operacao.Quantidade.ToString();
                    instCustom.ValorTitulo = Operacao.EspecificacaoTitulo.ToString();
                    instCustom.TabIndex = contTabIndex;
                    this.pnlListaDados.Controls.Add(instCustom);
                    this.pnlListaDados.Controls[this.pnlListaDados.Controls.Count - 1].Dock = DockStyle.Top;
                    contTabIndex--;
                    instCustom.Name = "CustomFieldsCarregarDados_" + contTabIndex;
                    instCustom.btnFechar.Name = "btnFechar_" + contTabIndex;
                    instCustom.btnFechar.Click += new EventHandler(UserControlID_buttonClick);
                }
            }
        }

        protected void UserControlID_buttonClick(object sender, EventArgs e)
        {
            string contIndex = ((Button)sender).Name.Split('_')[1];
            var x = this.pnlListaDados.Controls["CustomFieldsCarregarDados_" + contIndex];
            this.pnlListaDados.Controls.Remove(x);

            if (this.pnlListaDados.Controls.Count == 0)
            {
                this.pnlListaDados.Controls.Clear();
                this.txtDataPregao.Text = "";
                this.txtNumeroNota.Text = "";
                this.pnlDadosNota.Visible = false;
                this.ImportarDados(this.txtCaminho.Text);
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.pnlDadosNota.Visible == true)
                {
                    CtrlAtivos instCtrlAtivos = new CtrlAtivos();
                    EntDadosPdf instDadosPdf = new EntDadosPdf();
                    instDadosPdf.ListaOperacoes = new List<EntDadosOperacoesPdf>();

                    instDadosPdf.NumeroNota = int.Parse(this.txtNumeroNota.Text);
                    instDadosPdf.DataPregao = DateTime.Parse(this.txtDataPregao.Text);

                    foreach (var ControleDados in pnlListaDados.Controls.OfType<CustomFieldsCarregarDados>().ToList().OrderBy(x => x.TabIndex).ToList())
                    {
                        EntDadosOperacoesPdf instOperacao = new EntDadosOperacoesPdf();
                        if (!string.IsNullOrEmpty(ControleDados.ValorTicker))
                            instOperacao.EspecificacaoTitulo = ControleDados.ValorTicker;
                        else
                            throw new Exception("O campo 'Ativo' deve ser informado para todas as operações listadas");
                        instOperacao.TipoOperacao = ControleDados.ValorTipoOperacao;
                        instOperacao.EspecificacaoTitulo = ControleDados.ValorTitulo;
                        instOperacao.Quantidade = int.Parse(ControleDados.ValorQuantidade);
                        instOperacao.Preco = decimal.Parse(ControleDados.Valor.Replace("R$", ""));
                        instOperacao.ValorOperacao = decimal.Parse(ControleDados.ValorTotal.Replace("R$", ""));
                        instOperacao.Ativo = ControleDados.ValorTicker;
                        instDadosPdf.ListaOperacoes.Add(instOperacao);
                    }

                    instCtrlAtivos.TratarDadosInclusao(instDadosPdf);

                    MessageBox.Show("Operações Importadas Com Sucesso!.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.pnlListaDados.Controls.Clear();
                    this.AtualizarDataUltimoPregao();
                    this.txtDataPregao.Text = "";
                    this.txtNumeroNota.Text = "";
                    this.pnlDadosNota.Visible = false;
                    this.ImportarDados(this.txtCaminho.Text);
                }
                else
                    MessageBox.Show("É necessário carregar um arquivo para gravar os dados!.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao gravar os dados." + ex.Message);
            }
        }

        public void AtualizarDataUltimoPregao()
        {
            try
            {
                var dadosNota = this.instDadosNota.RecuperarUltimoPregaoImportado();
                if (dadosNota != null)
                    this.lblDataUltimoPregao.Text = dadosNota.DATA_NOTA.ToShortDateString();
                else
                    this.lblDataUltimoPregao.Text = "Não há dados importados";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao recuperar data do último pregão: " + ex.Message);
            }
        }
    }
}
