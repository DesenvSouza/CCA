﻿using Business.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCA.MenuLateral
{
    public partial class frmImportarRendimentos : Form
    {
        #region DECLARAÇÕES
        private const string DiretorioInicial = "c:\\";
        private const string ExtensaoArquivo = "pdf files (*.pdf)|*.pdf";

        CtrlLerArquivoPdf instLerArquivoPdf;
        #endregion

        public frmImportarRendimentos()
        {
            InitializeComponent();
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.IniciarOpenFile())
                {
                    this.instLerArquivoPdf.LerDadosRendimentosPdf(this.txtCaminho.Text);
                    MessageBox.Show("Dados Importados com Sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show("Nenhum arquivo selecionado.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool IniciarOpenFile()
        {
            try
            {
                using (OpenFileDialog ofdSelecionarArquivo = new OpenFileDialog())
                {

                    ofdSelecionarArquivo.InitialDirectory = DiretorioInicial;
                    ofdSelecionarArquivo.Filter = ExtensaoArquivo;
                    ofdSelecionarArquivo.FilterIndex = 0;

                    if (ofdSelecionarArquivo.ShowDialog() == DialogResult.OK)
                    {
                        this.txtCaminho.Text = ofdSelecionarArquivo.FileName;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void frmImportarRendimentos_Load(object sender, EventArgs e)
        {
            try
            {
                this.instLerArquivoPdf = new CtrlLerArquivoPdf();
            }
            catch
            {
                throw new Exception("Erro ao Iniciar o formulário!");
            }
        }
    }
}
