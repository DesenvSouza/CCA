﻿
namespace CCA.MenuLateral
{
    partial class frmImportarOperacoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDataUltimoPregao = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlDadosNota = new System.Windows.Forms.Panel();
            this.txtDataPregao = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumeroNota = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblValor = new System.Windows.Forms.Label();
            this.btnGravar = new System.Windows.Forms.Button();
            this.pnlListaDados = new System.Windows.Forms.Panel();
            this.txtCaminho = new System.Windows.Forms.TextBox();
            this.btnImportar = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.pnlDadosNota.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.panel1.Controls.Add(this.lblDataUltimoPregao);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.pnlDadosNota);
            this.panel1.Controls.Add(this.btnGravar);
            this.panel1.Controls.Add(this.pnlListaDados);
            this.panel1.Controls.Add(this.txtCaminho);
            this.panel1.Controls.Add(this.btnImportar);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(957, 635);
            this.panel1.TabIndex = 1;
            // 
            // lblDataUltimoPregao
            // 
            this.lblDataUltimoPregao.AutoSize = true;
            this.lblDataUltimoPregao.ForeColor = System.Drawing.Color.Silver;
            this.lblDataUltimoPregao.Location = new System.Drawing.Point(164, 604);
            this.lblDataUltimoPregao.Name = "lblDataUltimoPregao";
            this.lblDataUltimoPregao.Size = new System.Drawing.Size(0, 13);
            this.lblDataUltimoPregao.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Silver;
            this.label2.Location = new System.Drawing.Point(10, 604);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Data último pregão importado:";
            // 
            // pnlDadosNota
            // 
            this.pnlDadosNota.Controls.Add(this.txtDataPregao);
            this.pnlDadosNota.Controls.Add(this.panel3);
            this.pnlDadosNota.Controls.Add(this.label1);
            this.pnlDadosNota.Controls.Add(this.txtNumeroNota);
            this.pnlDadosNota.Controls.Add(this.panel8);
            this.pnlDadosNota.Controls.Add(this.lblValor);
            this.pnlDadosNota.Location = new System.Drawing.Point(13, 52);
            this.pnlDadosNota.Name = "pnlDadosNota";
            this.pnlDadosNota.Size = new System.Drawing.Size(931, 49);
            this.pnlDadosNota.TabIndex = 5;
            this.pnlDadosNota.Visible = false;
            // 
            // txtDataPregao
            // 
            this.txtDataPregao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.txtDataPregao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDataPregao.Enabled = false;
            this.txtDataPregao.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtDataPregao.ForeColor = System.Drawing.Color.Silver;
            this.txtDataPregao.Location = new System.Drawing.Point(626, 12);
            this.txtDataPregao.Name = "txtDataPregao";
            this.txtDataPregao.Size = new System.Drawing.Size(75, 16);
            this.txtDataPregao.TabIndex = 23;
            this.txtDataPregao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel3.Location = new System.Drawing.Point(626, 22);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(75, 10);
            this.panel3.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(541, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 15);
            this.label1.TabIndex = 21;
            this.label1.Text = "Data Pregão:";
            // 
            // txtNumeroNota
            // 
            this.txtNumeroNota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.txtNumeroNota.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumeroNota.Enabled = false;
            this.txtNumeroNota.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtNumeroNota.ForeColor = System.Drawing.Color.Silver;
            this.txtNumeroNota.Location = new System.Drawing.Point(276, 11);
            this.txtNumeroNota.Name = "txtNumeroNota";
            this.txtNumeroNota.Size = new System.Drawing.Size(65, 16);
            this.txtNumeroNota.TabIndex = 20;
            this.txtNumeroNota.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel8.Location = new System.Drawing.Point(276, 21);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(65, 10);
            this.panel8.TabIndex = 19;
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblValor.ForeColor = System.Drawing.Color.White;
            this.lblValor.Location = new System.Drawing.Point(186, 13);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(84, 15);
            this.lblValor.TabIndex = 18;
            this.lblValor.Text = "Número Nota:";
            // 
            // btnGravar
            // 
            this.btnGravar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.btnGravar.FlatAppearance.BorderSize = 0;
            this.btnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGravar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGravar.ForeColor = System.Drawing.Color.White;
            this.btnGravar.Location = new System.Drawing.Point(812, 594);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(132, 30);
            this.btnGravar.TabIndex = 4;
            this.btnGravar.Text = "Gravar Dados";
            this.btnGravar.UseVisualStyleBackColor = false;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // pnlListaDados
            // 
            this.pnlListaDados.Location = new System.Drawing.Point(13, 107);
            this.pnlListaDados.Name = "pnlListaDados";
            this.pnlListaDados.Size = new System.Drawing.Size(931, 473);
            this.pnlListaDados.TabIndex = 3;
            // 
            // txtCaminho
            // 
            this.txtCaminho.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.txtCaminho.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCaminho.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Italic);
            this.txtCaminho.ForeColor = System.Drawing.Color.Gray;
            this.txtCaminho.Location = new System.Drawing.Point(13, 15);
            this.txtCaminho.Name = "txtCaminho";
            this.txtCaminho.ReadOnly = true;
            this.txtCaminho.Size = new System.Drawing.Size(850, 25);
            this.txtCaminho.TabIndex = 0;
            this.txtCaminho.Text = "Carregar Arquivo...";
            // 
            // btnImportar
            // 
            this.btnImportar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.btnImportar.FlatAppearance.BorderSize = 0;
            this.btnImportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImportar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportar.ForeColor = System.Drawing.Color.White;
            this.btnImportar.Location = new System.Drawing.Point(869, 15);
            this.btnImportar.Name = "btnImportar";
            this.btnImportar.Size = new System.Drawing.Size(75, 30);
            this.btnImportar.TabIndex = 2;
            this.btnImportar.Text = "Carregar";
            this.btnImportar.UseVisualStyleBackColor = false;
            this.btnImportar.Click += new System.EventHandler(this.btnImportar_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.panel5.Location = new System.Drawing.Point(13, 40);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(850, 3);
            this.panel5.TabIndex = 1;
            // 
            // frmImportarOperacoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(44)))), ((int)(((byte)(68)))));
            this.ClientSize = new System.Drawing.Size(982, 660);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmImportarOperacoes";
            this.Text = "frmImportarOperacoes";
            this.Load += new System.EventHandler(this.frmImportarOperacoes_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlDadosNota.ResumeLayout(false);
            this.pnlDadosNota.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtCaminho;
        private System.Windows.Forms.Button btnImportar;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Panel pnlListaDados;
        private System.Windows.Forms.Panel pnlDadosNota;
        private System.Windows.Forms.TextBox txtDataPregao;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumeroNota;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.Label lblDataUltimoPregao;
        private System.Windows.Forms.Label label2;
    }
}