﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Business.Controllers;

namespace CCA.MenuLateral
{
    public partial class frmImportarDados : Form
    {
        #region DECLARAÇÕES
        private const string DiretorioInicial = "c:\\";
        private const string ExtensaoArquivo = "csv files (*.csv)|*.csv";

        CtrlLerArquivo instLerArquivo;
        #endregion

        public frmImportarDados()
        {
            InitializeComponent();
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.IniciarOpenFile())
                {
                    this.instLerArquivo.LerArquivo(this.txtCaminho.Text);
                    MessageBox.Show("Dados Importados com Sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show("Nenhum arquivo selecionado.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool IniciarOpenFile()
        {
            try
            {
                using (OpenFileDialog ofdSelecionarArquivo = new OpenFileDialog())
                {

                    ofdSelecionarArquivo.InitialDirectory = DiretorioInicial;
                    ofdSelecionarArquivo.Filter = ExtensaoArquivo;
                    ofdSelecionarArquivo.FilterIndex = 0;

                    if (ofdSelecionarArquivo.ShowDialog() == DialogResult.OK)
                    {
                        this.txtCaminho.Text = ofdSelecionarArquivo.FileName;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void frmImportarDados_Load(object sender, EventArgs e)
        {
            try
            {
                this.instLerArquivo = new CtrlLerArquivo();
            }
            catch
            {
                throw new Exception("Erro ao Iniciar o formulário!");
            }
        }
    }
}
