﻿using Business.Controllers;
using Business.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCA.MenuLateral
{
    public partial class frmAnaliseDados : Form
    {
        public frmAnaliseDados()
        {
            InitializeComponent();
        }

        private void frmAnaliseDados_Load(object sender, EventArgs e)
        {
            PreencherGridDadosTratados();
        }

        private void PreencherGridDadosTratados()
        {
            try
            {
                dgvDadosTratados.Columns.Clear();
                dgvDadosTratados.Rows.Clear();

                CtrlDadosBrutos instCtrlDadosBrutos = new CtrlDadosBrutos();
                List<EntDadosTratados> lstDadosTratados = instCtrlDadosBrutos.TratarDados();
                //dgvDadosTratados.DataSource = lstDadosTratados;

                DataSet dtsDados = new DataSet();
                dtsDados.Tables.Add("DadosTratados");

                dtsDados.Tables[0].Columns.Add("Ticker").DataType = System.Type.GetType("System.String");
                dtsDados.Tables[0].Columns.Add("Última Cotação").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Preço/Lucro").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Preço/Valor Patrimonial").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Dividend Yield").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Potencial Dividend Yield").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Earning Yield").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Preço Justo Graham").DataType = System.Type.GetType("System.Decimal");
                dtsDados.Tables[0].Columns.Add("Margem Segurança Graham").DataType = System.Type.GetType("System.Decimal");


                //dtsDados.Tables[0].Columns.Add("Preço Justo Variável").DataType = System.Type.GetType("System.Decimal");
                //dtsDados.Tables[0].Columns.Add("Margem Segurança Variável").DataType = System.Type.GetType("System.Decimal");

                if (lstDadosTratados.Count > 0)
                {

                    foreach (var Dados in lstDadosTratados.OrderByDescending(x => x.EarningYield)
                                                          .ThenByDescending(x => x.PotencialDividendYield)
                                                          .ThenByDescending(x => x.DividendYield)
                                                          .ThenBy(x => x.PrecoJusto22)
                                                          .ThenBy(x => x.PVP))
                    {
                        DataRow dtrLinha = dtsDados.Tables[0].NewRow();

                        dtrLinha[0] = Dados.Ticket;
                        dtrLinha[1] = decimal.Round(Dados.UltimaCotacao, 2);
                        dtrLinha[2] = decimal.Round(Dados.PL, 2);
                        dtrLinha[3] = decimal.Round(Dados.PVP, 2);
                        dtrLinha[4] = decimal.Round(Dados.DividendYield, 2);
                        dtrLinha[5] = decimal.Round(Dados.PotencialDividendYield, 2);
                        dtrLinha[6] = decimal.Round(Dados.EarningYield, 2);
                        dtrLinha[7] = decimal.Round(Dados.PrecoJusto22, 2);
                        dtrLinha[8] = decimal.Round(Dados.MargemSeguranca22, 2);
                        //dtrLinha[9] = decimal.Round(Dados.PrecoJustoVariaveis, 2);
                        //dtrLinha[10] = decimal.Round(Dados.MargemSegurancaVariavel, 2);


                        dtsDados.Tables[0].Rows.Add(dtrLinha);

                    }
                    dgvDadosTratados.DataSource = dtsDados.Tables[0];

                    dgvDadosTratados.Columns[1].DefaultCellStyle.Format = "c2";
                    dgvDadosTratados.Columns[4].DefaultCellStyle.Format = "0.00\\%";
                    dgvDadosTratados.Columns[5].DefaultCellStyle.Format = "0.00\\%";
                    dgvDadosTratados.Columns[6].DefaultCellStyle.Format = "0.00\\%";
                    dgvDadosTratados.Columns[7].DefaultCellStyle.Format = "c2";
                    dgvDadosTratados.Columns[8].DefaultCellStyle.Format = "0.00\\%";

                    dgvDadosTratados.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosTratados.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosTratados.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosTratados.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosTratados.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosTratados.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosTratados.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosTratados.Columns[7].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvDadosTratados.Columns[8].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPesquisar_TextChanged(object sender, EventArgs e)
        {
            TextBox txtPesquisar = (TextBox)sender;

            var dtbTabela = (DataTable)dgvDadosTratados.DataSource;

            if (dtbTabela != null)
            {
                dtbTabela.DefaultView.RowFilter = string.Format("Ticker like '%{0}%'", txtPesquisar.Text.Trim());
                dgvDadosTratados.Refresh();
            }
        }
    }
}
