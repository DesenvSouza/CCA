﻿
namespace CCA.MenuLateral
{
    partial class frmDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPosicaoInicial = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblResultadoConsolidado = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dgvDadosPosicaoAtual = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lbltextposicao = new System.Windows.Forms.Label();
            this.lblPosicaoAtual = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lblResultadoParcial = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDadosPosicaoAtual)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(342, 306);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(272, 51);
            this.label1.TabIndex = 0;
            this.label1.Text = "BEM VINDO";
            this.label1.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.panel1.Controls.Add(this.lblPosicaoInicial);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(220, 130);
            this.panel1.TabIndex = 1;
            // 
            // lblPosicaoInicial
            // 
            this.lblPosicaoInicial.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblPosicaoInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.lblPosicaoInicial.ForeColor = System.Drawing.Color.White;
            this.lblPosicaoInicial.Location = new System.Drawing.Point(0, 65);
            this.lblPosicaoInicial.Name = "lblPosicaoInicial";
            this.lblPosicaoInicial.Size = new System.Drawing.Size(220, 65);
            this.lblPosicaoInicial.TabIndex = 1;
            this.lblPosicaoInicial.Text = "Posição inicial";
            this.lblPosicaoInicial.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(0, 30, 0, 0);
            this.label2.Size = new System.Drawing.Size(220, 65);
            this.label2.TabIndex = 0;
            this.label2.Text = "Posição inicial";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.panel2.Controls.Add(this.lblResultadoConsolidado);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(232, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(220, 130);
            this.panel2.TabIndex = 2;
            // 
            // lblResultadoConsolidado
            // 
            this.lblResultadoConsolidado.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblResultadoConsolidado.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.lblResultadoConsolidado.ForeColor = System.Drawing.Color.White;
            this.lblResultadoConsolidado.Location = new System.Drawing.Point(0, 65);
            this.lblResultadoConsolidado.Name = "lblResultadoConsolidado";
            this.lblResultadoConsolidado.Size = new System.Drawing.Size(220, 65);
            this.lblResultadoConsolidado.TabIndex = 1;
            this.lblResultadoConsolidado.Text = "Posição atual";
            this.lblResultadoConsolidado.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(0, 30, 0, 0);
            this.label4.Size = new System.Drawing.Size(220, 65);
            this.label4.TabIndex = 0;
            this.label4.Text = "Resultado Consolidado";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(982, 660);
            this.panel3.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.panel5.Controls.Add(this.dgvDadosPosicaoAtual);
            this.panel5.Location = new System.Drawing.Point(20, 156);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(10);
            this.panel5.Size = new System.Drawing.Size(942, 492);
            this.panel5.TabIndex = 2;
            // 
            // dgvDadosPosicaoAtual
            // 
            this.dgvDadosPosicaoAtual.AllowUserToAddRows = false;
            this.dgvDadosPosicaoAtual.AllowUserToDeleteRows = false;
            this.dgvDadosPosicaoAtual.AllowUserToResizeColumns = false;
            this.dgvDadosPosicaoAtual.AllowUserToResizeRows = false;
            this.dgvDadosPosicaoAtual.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDadosPosicaoAtual.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(44)))), ((int)(((byte)(68)))));
            this.dgvDadosPosicaoAtual.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDadosPosicaoAtual.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDadosPosicaoAtual.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvDadosPosicaoAtual.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(44)))), ((int)(((byte)(68)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDadosPosicaoAtual.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvDadosPosicaoAtual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDadosPosicaoAtual.EnableHeadersVisualStyles = false;
            this.dgvDadosPosicaoAtual.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(66)))), ((int)(((byte)(109)))));
            this.dgvDadosPosicaoAtual.Location = new System.Drawing.Point(10, 10);
            this.dgvDadosPosicaoAtual.MultiSelect = false;
            this.dgvDadosPosicaoAtual.Name = "dgvDadosPosicaoAtual";
            this.dgvDadosPosicaoAtual.ReadOnly = true;
            this.dgvDadosPosicaoAtual.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(44)))), ((int)(((byte)(68)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDadosPosicaoAtual.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvDadosPosicaoAtual.RowHeadersVisible = false;
            this.dgvDadosPosicaoAtual.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvDadosPosicaoAtual.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDadosPosicaoAtual.ShowEditingIcon = false;
            this.dgvDadosPosicaoAtual.Size = new System.Drawing.Size(922, 472);
            this.dgvDadosPosicaoAtual.TabIndex = 1;
            this.dgvDadosPosicaoAtual.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvDadosPosicaoAtual_CellFormatting);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(44)))), ((int)(((byte)(68)))));
            this.panel4.Controls.Add(this.panel9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(510, 0);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(0, 20, 20, 0);
            this.panel4.Size = new System.Drawing.Size(472, 660);
            this.panel4.TabIndex = 5;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(44)))), ((int)(((byte)(68)))));
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(20, 20, 0, 0);
            this.panel6.Size = new System.Drawing.Size(472, 660);
            this.panel6.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.panel1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(20, 20);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(452, 130);
            this.panel7.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.panel8.Controls.Add(this.lblPosicaoAtual);
            this.panel8.Controls.Add(this.lbltextposicao);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(232, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(220, 130);
            this.panel8.TabIndex = 2;
            // 
            // lbltextposicao
            // 
            this.lbltextposicao.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbltextposicao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lbltextposicao.ForeColor = System.Drawing.Color.White;
            this.lbltextposicao.Location = new System.Drawing.Point(0, 0);
            this.lbltextposicao.Name = "lbltextposicao";
            this.lbltextposicao.Padding = new System.Windows.Forms.Padding(0, 30, 0, 0);
            this.lbltextposicao.Size = new System.Drawing.Size(220, 65);
            this.lbltextposicao.TabIndex = 1;
            this.lbltextposicao.Text = "Posição Atual";
            this.lbltextposicao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPosicaoAtual
            // 
            this.lblPosicaoAtual.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblPosicaoAtual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.lblPosicaoAtual.ForeColor = System.Drawing.Color.White;
            this.lblPosicaoAtual.Location = new System.Drawing.Point(0, 65);
            this.lblPosicaoAtual.Name = "lblPosicaoAtual";
            this.lblPosicaoAtual.Size = new System.Drawing.Size(220, 65);
            this.lblPosicaoAtual.TabIndex = 2;
            this.lblPosicaoAtual.Text = "PosicaoAtual";
            this.lblPosicaoAtual.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.panel2);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 20);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(452, 130);
            this.panel9.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(73)))));
            this.panel10.Controls.Add(this.lblResultadoParcial);
            this.panel10.Controls.Add(this.label5);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(220, 130);
            this.panel10.TabIndex = 3;
            // 
            // lblResultadoParcial
            // 
            this.lblResultadoParcial.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblResultadoParcial.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.lblResultadoParcial.ForeColor = System.Drawing.Color.White;
            this.lblResultadoParcial.Location = new System.Drawing.Point(0, 65);
            this.lblResultadoParcial.Name = "lblResultadoParcial";
            this.lblResultadoParcial.Size = new System.Drawing.Size(220, 65);
            this.lblResultadoParcial.TabIndex = 3;
            this.lblResultadoParcial.Text = "Resultado Parcial";
            this.lblResultadoParcial.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(0, 30, 0, 0);
            this.label5.Size = new System.Drawing.Size(220, 65);
            this.label5.TabIndex = 2;
            this.label5.Text = "Resultado Parcial";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(44)))), ((int)(((byte)(68)))));
            this.ClientSize = new System.Drawing.Size(982, 660);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmDashboard";
            this.Text = "frmDashboard";
            this.Load += new System.EventHandler(this.frmDashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDadosPosicaoAtual)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPosicaoInicial;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblResultadoConsolidado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView dgvDadosPosicaoAtual;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblPosicaoAtual;
        private System.Windows.Forms.Label lbltextposicao;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label lblResultadoParcial;
        private System.Windows.Forms.Label label5;
    }
}