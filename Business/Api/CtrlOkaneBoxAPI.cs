﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Dados.Controllers;
using Business.Entidades;

namespace Business.Api
{
    public class CtrlOkaneBoxAPI
    {
        public const string UrlBase = "https://www.okanebox.com.br/api/analisefundamentalista/";

        public void AtualizarDadosBrutos()
        {
            try
            {
                AtivosController instAtivos = new AtivosController();
                var lstAtivos = instAtivos.RecuperarRegistros();

                foreach (var Ativo in lstAtivos)
                {
                    this.RecuperarDadosAtivos(Ativo.CODIGO);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RecuperarDadosAtivos(string CodigoAtivo)
        {
            HttpClient client = new HttpClient();
            var response = client.GetAsync(UrlBase + CodigoAtivo + "/").Result;
            var result = response.Content.ReadAsStringAsync().Result;
            var resultadoFinal = JsonConvert.DeserializeObject<List<EntDadosApiOkane>>(result);
        }

    }
}
