﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entidades
{
    public class EntDadosPdf
    {
        public DateTime DataPregao { get; set; }

        public int NumeroNota { get; set; }

        public List<EntDadosOperacoesPdf> ListaOperacoes { get; set; }
    }
}
