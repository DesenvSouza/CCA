﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entidades
{
    public class EntDadosRendimentosPdf
    {
        public string Ativo { get; set; }

        public DateTime DataPagamento { get; set; }

        public int Quantidade { get; set; }

        public decimal ValorBruto { get; set; }

        public decimal ValorIr { get; set; }

        public decimal ValorLiquido { get; set; }
    }
}
