﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entidades
{
    public class EntDadosOperacoesPdf
    {
        public string TipoOperacao { get; set; }

        public string TipoMercado { get; set; }

        public string EspecificacaoTitulo { get; set; }

        public int Quantidade { get; set; }

        public decimal Preco { get; set; }
        
        public decimal ValorOperacao{ get; set; }

        public int Index { get; set; }

        public string Ativo { get; set; }
    }
}
