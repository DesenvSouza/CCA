﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entidades
{
    public class EntDadosTratados
    {
        public string Ticket { get; set; }
        public decimal UltimaCotacao { get; set; }
        public decimal DividendYield { get; set; }
        public decimal PL { get; set; }
        public decimal PVP { get; set; }
        public decimal ROE { get; set; }
        public decimal ROIC { get; set; }
        public decimal LiquidezMediaDiaria { get; set; }
        public decimal EarningYield { get; set; }
        public decimal PotencialDividendYield { get; set; }
        public decimal PrecoJusto22 { get; set; }
        public decimal PrecoJustoVariaveis { get; set; }
        public decimal MargemSeguranca22 { get; set; }
        public decimal MargemSegurancaVariavel { get; set; }
        public decimal PVPA { get; set; }


        //public decimal RetornoSobreCapital { get; set; }
    }
}
