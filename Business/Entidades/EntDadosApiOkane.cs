﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entidades
{
    public class EntDadosApiOkane
    {
        public string title { get; set; }
        public double? value { get; set; }
        public string details { get; set; }
    }
}
