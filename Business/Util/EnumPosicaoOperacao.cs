﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Util
{
    public enum EnumPosicaoOperacao
    {
        TipoOperacao = 1,

        TipoMercado = 2,

        EspecificacaoTitulo = 3,

        Quantidade = 4,

        Preco = 5,

        ValorOperacao = 6
    }
}
