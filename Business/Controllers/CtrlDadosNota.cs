﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Controllers;
using Dados.Database;
using Business.Entidades;

namespace Business.Controllers
{
    public class CtrlDadosNota : DadosNotaController
    {
        public List<DADOS_NOTA> ListarNotas(int NumeroNota)
        {
            DadosNotaController instDadosNota = new DadosNotaController();
            return instDadosNota.RecuperarRegistros(NumeroNota);
        }

        public void InserirNota(EntDadosPdf DadosPdf)
        {
            DADOS_NOTA EntDadosNota = new DADOS_NOTA();
            DadosNotaController instDadosNota = new DadosNotaController();

            EntDadosNota.NUMERO_NOTA = DadosPdf.NumeroNota;
            EntDadosNota.DATA_NOTA = DadosPdf.DataPregao;
            EntDadosNota.DATA_IMPORTACAO = DateTime.Now;

            instDadosNota.InserirRegistro(EntDadosNota);
        }
    }
}
