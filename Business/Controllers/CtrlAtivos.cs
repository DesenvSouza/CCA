﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Controllers;
using Dados.Database;
using Business.Entidades;
using Dados.Util;
using System.Transactions;

namespace Business.Controllers
{
    public class CtrlAtivos : AtivosController
    {
        public void TratarDadosInclusao(EntDadosPdf DadosPdf)
        {
            try
            {
                CtrlDadosNota instDadosNota = new CtrlDadosNota();
                CtrlOperacoes instOperacoes = new CtrlOperacoes();
                CtrlDescricaoAtivos instDescricaoAtivos = new CtrlDescricaoAtivos();    

                instDadosNota.InserirNota(DadosPdf);

                foreach (EntDadosOperacoesPdf Operacao in DadosPdf.ListaOperacoes)
                {
                    int idAtivo = this.InserirAtivo(Operacao.Ativo, Operacao.EspecificacaoTitulo);
                    instDescricaoAtivos.TratarDadosInsercao(idAtivo, Operacao.EspecificacaoTitulo);

                    instOperacoes.InserirOperacoes(Operacao, DadosPdf.NumeroNota);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ATIVOS> BuscarAtivos(string DescricaoAtivo = null,
                                            string Ticker = null,
                                                EnumTipoOperador.TipoOperador TipoOperador = EnumTipoOperador.TipoOperador.And)
        {
            try
            {
                List<ATIVOS> lstAtivos = this.RecuperarRegistros(DescricaoAtivo, Ticker, TipoOperador);
                return lstAtivos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int InserirAtivo(string Ticker, string DescricaoAtivo)
        {
            try
            {
                if (this.BuscarAtivos(DescricaoAtivo, Ticker, EnumTipoOperador.TipoOperador.Or).Count <= 0)
                {
                    ATIVOS instDadosAtivos = new ATIVOS();
                    instDadosAtivos.CODIGO = Ticker;
                    instDadosAtivos.DESCRICAO_ATIVO = DescricaoAtivo;
                    instDadosAtivos.DATA_CRIACAO = DateTime.Now;
                    instDadosAtivos.ID_STATUS = 1;
                    instDadosAtivos.ID_EMPRESA = 1;
                    return this.InserirRegistro(instDadosAtivos);
                }
                else
                {
                    ATIVOS Ativo = this.BuscarAtivos(DescricaoAtivo, Ticker, EnumTipoOperador.TipoOperador.Or)[0];
                    Ativo.DESCRICAO_ATIVO = DescricaoAtivo.ToUpper();
                    this.AtualizarRegistro(Ativo);
                    return Ativo.ID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
