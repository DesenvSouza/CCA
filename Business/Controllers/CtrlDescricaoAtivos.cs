﻿using Dados.Controllers;
using Dados.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Controllers
{
    public class CtrlDescricaoAtivos : DescricaoAtivosController
    {
        public void TratarDadosInsercao(int idAtivo, string EspecificacaoTitulo)
        {
            try
            {
                DESCRICAO_ATIVOS instDescricaoAtivo = new DESCRICAO_ATIVOS();
                instDescricaoAtivo.ID_ATIVO = idAtivo;
                instDescricaoAtivo.DESCRICAO_ATIVO = EspecificacaoTitulo;
                if (RecuperarRegistro(0, EspecificacaoTitulo).Count == 0)
                    this.InserirRegistro(instDescricaoAtivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
