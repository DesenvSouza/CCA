﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Business.Entidades;
using System;
using Business.Util;
using Spire.Pdf;
using Spire.Pdf.Exporting.Text;

namespace Business.Controllers
{
    public class CtrlLerArquivoPdf
    {
        #region CONSTANTES
        private const string DataPregao = "Data pregão";
        private const string NumeroNota = "Nr. nota";
        private const string Operacoes = "1-BOVESPA";

        private const string Cabecalho = "Ativo Evento Quantidade Valor Bruto (R$) Valor IR (R$) Valor Líquido (R$) Pagamento";
        private const string InformacoesComplementares = "INFORMAÇÕES COMPLEMENTARES";
        #endregion

        public EntDadosPdf LerArquivoOperacoesPdf(string fileName)
        {
            if (File.Exists(fileName))
            {

                PdfDocument document = new PdfDocument();

                document.LoadFromFile(fileName);
                SimpleTextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                var content = document.Pages[0].ExtractText(strategy).Replace("\n", "").Split(new[] { '\r' });
                return TratarDadosOperacoesPdf(content.ToList());
            }
            else
            {
                throw new FileNotFoundException("Arquivo Inexistente");
            }
        }

        public void LerDadosRendimentosPdf(string fileName)
        {
            try
            {
                PdfDocument document = new PdfDocument();

                document.LoadFromFile(fileName);
                SimpleTextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                List<EntDadosRendimentosPdf> ListaDadosRendimentos = new List<EntDadosRendimentosPdf>();

                for (int cont = 0; cont <= document.Pages.Count; cont++)
                {
                    var content = document.Pages[cont].ExtractText(strategy).Replace("\n", "").Split(new[] { '\r' });

                    var listaRendimentos = content.ToList();
                    listaRendimentos.RemoveRange(0, content.ToList().IndexOf(Cabecalho) + 1);

                    if (listaRendimentos[0].ToString() == InformacoesComplementares)
                        break;

                    for (int i = 0; i < listaRendimentos.Count; i++)
                    {
                        EntDadosRendimentosPdf instDadosRendimentos = new EntDadosRendimentosPdf();

                        instDadosRendimentos.Ativo = listaRendimentos[i].ToString();

                        if (string.IsNullOrEmpty(instDadosRendimentos.Ativo))
                            break;

                        i = i + 2;
                        if (!instDadosRendimentos.Ativo.Contains("TOTAL"))
                        {
                            var ValoresLinha = listaRendimentos[i].Split(' ');

                            if (!string.IsNullOrEmpty(ValoresLinha[0].ToString())
                                && int.TryParse(ValoresLinha[0].ToString().Replace(".", ""), out int Quantidade))
                            {
                                instDadosRendimentos.Quantidade = Quantidade;
                            }
                            if (!string.IsNullOrEmpty(ValoresLinha[1].ToString())
                                && decimal.TryParse(ValoresLinha[1].ToString(), out decimal ValorBruto))
                            {
                                instDadosRendimentos.ValorBruto = ValorBruto;
                            }
                            if (!string.IsNullOrEmpty(ValoresLinha[2].ToString())
                                && decimal.TryParse(ValoresLinha[2].ToString(), out decimal ValorIr))
                            {
                                instDadosRendimentos.ValorIr = ValorIr;
                            }
                            if (!string.IsNullOrEmpty(ValoresLinha[3].ToString())
                                && decimal.TryParse(ValoresLinha[3].ToString(), out decimal ValorLiquido))
                            {
                                instDadosRendimentos.ValorLiquido = ValorLiquido;
                            }
                            if (!string.IsNullOrEmpty(ValoresLinha[4].ToString())
                                && DateTime.TryParse(ValoresLinha[4].ToString(), out DateTime DataPagamento))
                            {
                                instDadosRendimentos.DataPagamento = DataPagamento;
                            }
                            ListaDadosRendimentos.Add(instDadosRendimentos);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private EntDadosPdf TratarDadosOperacoesPdf(List<string> ConteudoPdf)
        {
            EntDadosPdf instEntDadosPdf = new EntDadosPdf();

            instEntDadosPdf.DataPregao = DateTime.Parse(ConteudoPdf[ConteudoPdf.IndexOf(DataPregao) + 1].ToString());
            instEntDadosPdf.NumeroNota = int.Parse(ConteudoPdf[ConteudoPdf.IndexOf(NumeroNota) + 1].ToString());
            instEntDadosPdf.ListaOperacoes = new List<EntDadosOperacoesPdf>();

            int Index = 0;
            foreach (string Linha in ConteudoPdf)
            {
                if (Linha.StartsWith(Operacoes))
                {
                    EntDadosOperacoesPdf instDadosOperacoes = new EntDadosOperacoesPdf();

                    string[] ConteudoLinha = Linha.Split(' ');
                    int intAcrescimo = 0;
                    instDadosOperacoes.Index = Index++;
                    instDadosOperacoes.TipoOperacao = ConteudoLinha[(int)EnumPosicaoOperacao.TipoOperacao].ToString() == "V" ? "VENDA" : "COMPRA";
                    instDadosOperacoes.TipoMercado = ConteudoLinha[(int)EnumPosicaoOperacao.TipoMercado].ToString();
                    instDadosOperacoes.EspecificacaoTitulo = ConteudoLinha[(int)EnumPosicaoOperacao.EspecificacaoTitulo].ToString();
                    int result = 0;
                    while (int.TryParse(ConteudoLinha[(int)EnumPosicaoOperacao.Quantidade + intAcrescimo].ToString().Replace(".", ""), out result) != true)
                    {
                        string ValorTratado = ConteudoLinha[(int)EnumPosicaoOperacao.Quantidade + intAcrescimo].ToString().TrimStart();
                        if (!string.IsNullOrEmpty(ValorTratado))
                            instDadosOperacoes.EspecificacaoTitulo += " " + ValorTratado;
                        intAcrescimo++;
                    }
                    instDadosOperacoes.Quantidade = int.Parse(ConteudoLinha[(int)EnumPosicaoOperacao.Quantidade + intAcrescimo].ToString().Replace(".", ""));
                    instDadosOperacoes.Preco = decimal.Parse(ConteudoLinha[(int)EnumPosicaoOperacao.Preco + intAcrescimo].ToString());
                    instDadosOperacoes.ValorOperacao = decimal.Parse(ConteudoLinha[(int)EnumPosicaoOperacao.ValorOperacao + intAcrescimo].ToString());

                    instEntDadosPdf.ListaOperacoes.Add(instDadosOperacoes);
                }
            }
            //instEntDadosPdf.ListaOperacoes = instEntDadosPdf.ListaOperacoes.OrderByDescending(x => x.Index).ToList(); 
            return instEntDadosPdf;
        }
    }



    /// CÓDIGO COM IRON PDF
    /// 
    //    public EntDadosPdf LerArquivoPdf(string fileName)
    //    {
    //        if (File.Exists(fileName))
    //        {
    //            PdfDocument pdfArquivo = PdfDocument.FromFile(fileName);
    //            string[] strConteudoPdf = pdfArquivo.ExtractAllText().Replace("\n", "").Split(new[] { '\r' });
    //            return TratarDadosPdf(strConteudoPdf.ToList());
    //        }
    //        else
    //        {
    //            throw new FileNotFoundException("Arquivo Inexistente");
    //        }
    //    }

    //    private EntDadosPdf TratarDadosPdf(List<string> ConteudoPdf)
    //    {
    //        EntDadosPdf instEntDadosPdf = new EntDadosPdf();

    //        instEntDadosPdf.DataPregao = DateTime.Parse(ConteudoPdf[ConteudoPdf.IndexOf(DataPregao) + 1].ToString()); 
    //        instEntDadosPdf.NumeroNota = int.Parse(ConteudoPdf[ConteudoPdf.IndexOf(NumeroNota) + 1].ToString());
    //        instEntDadosPdf.ListaOperacoes = new List<EntDadosOperacoesPdf>();

    //        foreach(string Linha in ConteudoPdf)
    //        {
    //            if (Linha.StartsWith(Operacoes))
    //            {
    //                EntDadosOperacoesPdf instDadosOperacoes = new EntDadosOperacoesPdf();

    //                string[] ConteudoLinha = Linha.Split(' ');
    //                int intAcrescimo = 0;
    //                instDadosOperacoes.TipoOperacao = ConteudoLinha[(int)EnumPosicaoOperacao.TipoOperacao].ToString() == "V" ? "VENDA" : "COMPRA";
    //                instDadosOperacoes.TipoMercado = ConteudoLinha[(int)EnumPosicaoOperacao.TipoMercado].ToString();
    //                instDadosOperacoes.EspecificacaoTitulo = ConteudoLinha[(int)EnumPosicaoOperacao.EspecificacaoTitulo].ToString();
    //                int result = 0;
    //                while (int.TryParse(ConteudoLinha[(int)EnumPosicaoOperacao.Quantidade + intAcrescimo].ToString(), out result) != true)
    //                {
    //                    instDadosOperacoes.EspecificacaoTitulo += " " + ConteudoLinha[(int)EnumPosicaoOperacao.Quantidade + intAcrescimo].ToString();
    //                    intAcrescimo++;
    //                }
    //                instDadosOperacoes.Quantidade = int.Parse(ConteudoLinha[(int)EnumPosicaoOperacao.Quantidade + intAcrescimo].ToString());
    //                instDadosOperacoes.Preco = decimal.Parse(ConteudoLinha[(int)EnumPosicaoOperacao.Preco + intAcrescimo].ToString());
    //                instDadosOperacoes.ValorOperacao = decimal.Parse(ConteudoLinha[(int)EnumPosicaoOperacao.ValorOperacao + intAcrescimo].ToString());

    //                instEntDadosPdf.ListaOperacoes.Add(instDadosOperacoes);
    //            }
    //        }
    //        return instEntDadosPdf;
    //    }
    //}
}