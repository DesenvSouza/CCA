﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Controllers;
using Dados.Database;
using Business.Entidades;

namespace Business.Controllers
{
    public class CtrlDadosBrutos
    {
        public List<DADOS_BRUTOS> LerDadosBrutos(string Ticker = null)
        {
            try
            {
                LoteDadosBrutosController instLoteDadosBrutos = new LoteDadosBrutosController();
                DadosBrutosController instDadosBrutos = new DadosBrutosController();
                var MaiorLote = instLoteDadosBrutos.RecuperarUltimoLote();
                List<DADOS_BRUTOS> lstDadosBrutos = new List<DADOS_BRUTOS>();
                if (MaiorLote != null)
                    lstDadosBrutos = instDadosBrutos.RecuperarRegistros(MaiorLote.ID, Ticker);

                return lstDadosBrutos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EntDadosTratados> TratarDados(string Ticker = null)
        {
            try
            {
                List<DADOS_BRUTOS> lstDadosBrutos = this.FiltrarDados(LerDadosBrutos(Ticker));
                List<EntDadosTratados> lstDadosTratados = new List<EntDadosTratados>();

                foreach (DADOS_BRUTOS Registro in lstDadosBrutos)
                {
                    EntDadosTratados instDadosTratados = new EntDadosTratados();
                    instDadosTratados.Ticket = Registro.TICKER;
                    instDadosTratados.UltimaCotacao = Registro.PRECO.Value;
                    instDadosTratados.DividendYield = Registro.DIVIDEND_YIELD.Value;
                    instDadosTratados.PL = Registro.P_L.Value;
                    instDadosTratados.PVP = Registro.P_VP.Value;
                    instDadosTratados.ROE = Registro.ROE.Value;
                    instDadosTratados.ROIC = Registro.ROIC.Value;
                    instDadosTratados.LiquidezMediaDiaria = Registro.LIQUIDEZ_MEDIA_DIARIA.Value;
                    if (Registro.LPA.Value > 0 && Registro.PRECO.Value > 0)
                        instDadosTratados.EarningYield = (Registro.LPA.Value / Registro.PRECO.Value) * 100;
                    if (Registro.P_L.Value > 0)
                        instDadosTratados.PotencialDividendYield = 100 / Registro.P_L.Value;
                    if (Registro.PRECO.Value > 0 && Registro.VPA.Value > 0)
                        instDadosTratados.PVPA = Registro.PRECO.Value / Registro.VPA.Value;

                    if (Registro.PRECO > 0 && Registro.VPA > 0)
                    {
                        if (Registro.P_L.Value < 15 && ((Registro.PRECO / Registro.VPA) < decimal.Parse("1,5")))
                        {
                            if (Registro.LPA.Value > 0 && Registro.VPA.Value > 0)
                                instDadosTratados.PrecoJusto22 = decimal.Parse(Math.Sqrt(double.Parse((decimal.Parse("22,5") * Registro.LPA * Registro.VPA).ToString())).ToString());
                            if (Registro.P_L.Value > 0 && Registro.P_VP > 0 && Registro.LPA.Value > 0)
                                instDadosTratados.PrecoJustoVariaveis = decimal.Parse(Math.Sqrt(double.Parse(((Registro.P_L.Value * Registro.P_VP) * Registro.LPA * Registro.VPA).ToString())).ToString());
                            if (instDadosTratados.PrecoJusto22 > 0 && instDadosTratados.UltimaCotacao > 0)
                                instDadosTratados.MargemSeguranca22 = ((instDadosTratados.PrecoJusto22 - instDadosTratados.UltimaCotacao) / instDadosTratados.PrecoJusto22) * 100;
                            if (instDadosTratados.PrecoJustoVariaveis > 0 && instDadosTratados.UltimaCotacao > 0)
                                instDadosTratados.MargemSegurancaVariavel = ((instDadosTratados.PrecoJustoVariaveis - instDadosTratados.UltimaCotacao) / instDadosTratados.PrecoJustoVariaveis) * 100;

                        }
                        else
                        {
                            instDadosTratados.PrecoJustoVariaveis = 0;
                            instDadosTratados.PrecoJusto22 = 0;
                            instDadosTratados.MargemSeguranca22 = 0;
                            instDadosTratados.MargemSegurancaVariavel = 0;
                        }
                    }
                    //instDadosTratados.RetornoSobreCapital = Registro.MARGEM_EBIT.Value / Registro.PRECO_CAPITAL_DE_GIRO.Value;

                    lstDadosTratados.Add(instDadosTratados);
                }

                return lstDadosTratados;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<DADOS_BRUTOS> FiltrarDados(List<DADOS_BRUTOS> ListaDadosBrutos)
        {
            try
            {

                ParametrosFiltros instParametrosFiltros = new ParametrosFiltros();

                var lstParametros = instParametrosFiltros.RecuperarRegistros();

                ListaDadosBrutos.Remove(ListaDadosBrutos.Find(x => x.TICKER == "BCFF11"));

                foreach (var Parametro in lstParametros)
                {
                    List<DADOS_BRUTOS> lstNovaLista = new List<DADOS_BRUTOS>();

                    //Operadores invertidos pois os parametros estão removendo os registros da lista ao invés de adiciona-los
                    if (Parametro.TIPO_OPERADOR == "MAIOR")
                        lstNovaLista.AddRange(ListaDadosBrutos.FindAll(x => (decimal)(x.GetType().GetProperty(Parametro.CAMPO_VALIDACAO).GetValue(x, null)) < Parametro.VALOR_VALIDACAO));
                    else
                        lstNovaLista.AddRange(ListaDadosBrutos.FindAll(x => (decimal)(x.GetType().GetProperty(Parametro.CAMPO_VALIDACAO).GetValue(x, null)) > Parametro.VALOR_VALIDACAO));

                    ListaDadosBrutos = ListaDadosBrutos.Except(lstNovaLista).ToList();
                }

                return ListaDadosBrutos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
