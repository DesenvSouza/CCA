﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dados.Controllers;
using Dados.Database;
using Business.Entidades;

namespace Business.Controllers
{
    public class CtrlOperacoes : OperacoesController
    {
        public void InserirOperacoes(EntDadosOperacoesPdf Operacao, int NumeroNota)
        {
            OPERACOES EntOperacao = new OPERACOES();
            DadosNotaController instDadosNota = new DadosNotaController();
            AtivosController instAtivos = new AtivosController();
            TipoOperacaoController instTipoOperacao = new TipoOperacaoController();
            DADOS_NOTA EntDadosNota = new DADOS_NOTA();
            EntDadosNota = instDadosNota.RecuperarRegistros(NumeroNota)[0];

            EntOperacao.ID_NOTA = EntDadosNota.ID;
            EntOperacao.ID_TIPO_OPERACAO = instTipoOperacao.RecuperarRegistros(Operacao.TipoOperacao)[0].ID;
            EntOperacao.ID_ATIVO = instAtivos.RecuperarRegistros(null, Operacao.Ativo)[0].ID;
            EntOperacao.QUANTIDADE = Operacao.Quantidade;
            EntOperacao.VALOR_ATIVO = Operacao.Preco;
            EntOperacao.VALOR_OPERACAO = Operacao.ValorOperacao;
            EntOperacao.DATA_OPERACAO = EntDadosNota.DATA_NOTA;
            EntOperacao.DATA_CRIACAO = DateTime.Now;

            this.InserirRegistro(EntOperacao);
        }

    }
}
