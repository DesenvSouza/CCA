﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dados.Controllers;
using Dados.Database;

namespace Business.Controllers
{
    public class CtrlLerArquivo
    {
        public void LerArquivo(string CaminhoArquivo)
        {
            try
            {
                CCAEntities I = new CCAEntities();
                DADOS_BRUTOS instDadosAtivos = new DADOS_BRUTOS();

                LoteDadosBrutosController instLoteDadosBrutos = new LoteDadosBrutosController();
                DadosBrutosController instDadosBrutos = new DadosBrutosController();
                using (TransactionScope scope = new TransactionScope())
                {
                    using (var Arquivo = new StreamReader(CaminhoArquivo))
                    {
                        int intIdLote = instLoteDadosBrutos.InserirRegistro();
                        while (!Arquivo.EndOfStream)
                        {
                            var Linha = Arquivo.ReadLine();
                            if (!Linha.StartsWith("TICKER"))
                            {
                                instDadosAtivos = this.MontarEstrutura(Linha);
                                instDadosAtivos.ID_LOTE = intIdLote;
                                if (instDadosAtivos.PRECO > 0)
                                    instDadosBrutos.InserirRegistro(instDadosAtivos);
                            }
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ValidarDadosInsert(DADOS_BRUTOS DadosBrutos)
        {
            try
            {
                foreach (var Propriedade in DadosBrutos.GetType().GetProperties())
                {
                    if (Propriedade.PropertyType == typeof(Nullable<decimal>))
                    {
                        if (Propriedade.Name == "PRECO" && (decimal)Propriedade.GetValue(DadosBrutos, null) <= 0)
                            return false;

                        if ((decimal)Propriedade.GetValue(DadosBrutos, null) < 0 && Propriedade.Name != "PRECO_ATIVO_CIRCULANTE_LIQUIDO")
                            return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DADOS_BRUTOS MontarEstrutura(string ConteudoLinha)
        {
            try
            {
                int contLinha = 0;
                DADOS_BRUTOS instDados = new DADOS_BRUTOS();
                string[] PosicoesLinha = ConteudoLinha.Split(';');
                PosicoesLinha = PosicoesLinha.Select(Valor => string.IsNullOrWhiteSpace(Valor) ? "0" : Valor).ToArray();
                instDados.TICKER = PosicoesLinha[contLinha++];
                instDados.PRECO = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.DIVIDEND_YIELD = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.P_L = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.P_VP = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.P_ATIVOS = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.MARGEM_BRUTA = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.MARGEM_EBIT = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.MARGEM_LIQUIDA = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.PRECO_EBIT = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.EV_EBIT = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.DIVIDA_LIQUIDA_EBIT = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.DIVIDA_LIQUIDA_PATRIMONIO = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.PSR = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.PRECO_CAPITAL_DE_GIRO = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.PRECO_ATIVO_CIRCULANTE_LIQUIDO = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.LIQUIDO_CORRENTE = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.ROE = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.ROA = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.ROIC = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.PATRIMONIO_ATIVOS = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.PASSIVO_ATIVOS = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.GIRO_ATIVOS = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.CAGR_RECEITA_5_ANOS = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.CAGR_LUCRO_5_ANOS = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.LIQUIDEZ_MEDIA_DIARIA = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.VPA = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.LPA = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.PEG_RATIO = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                instDados.VALOR_DE_MERCADO = decimal.Parse(PosicoesLinha[contLinha++].ToString());
                return instDados;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
