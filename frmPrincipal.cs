﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCA.MenuLateral;
using CCA.Util;
using Business.Api;
using CCA.MenuAjuda;

namespace CCA
{
    public partial class frmPrincipal : Form
    {
        #region IMPORTS DLL
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        #endregion

        #region CONSTRUTORES
        public frmPrincipal()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            //Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 10, 10));
        }
        #endregion

        #region EVENTOS
        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            //this.AtualizarDadosBrutos(); 

            this.btnDashboard_Click(btnDashboard, e);
        }

        private void AtualizarDadosBrutos()
        {
            try
            {
                CtrlOkaneBoxAPI instOkaneBoxApi = new CtrlOkaneBoxAPI();
                instOkaneBoxApi.AtualizarDadosBrutos();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void pnlBarraTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAnaliseDados_Click(object sender, EventArgs e)
        {
            Button instBtnAcionado = (Button)sender;
            TratarBotoes(instBtnAcionado);
            CarregarFormPanel(instBtnAcionado.Name);
        }

        private void btnDashboard_Click(object sender, EventArgs e)
        {
            Button instBtnAcionado = (Button)sender;
            TratarBotoes(instBtnAcionado);
            CarregarFormPanel(instBtnAcionado.Name);
        }

        private void btnImportarDados_Click(object sender, EventArgs e)
        {
            Button instBtnAcionado = (Button)sender;
            TratarBotoes(instBtnAcionado);
            CarregarFormPanel(instBtnAcionado.Name);
        }

        private void btnImportarOperacoes_Click(object sender, EventArgs e)
        {
            Button instBtnAcionado = (Button)sender;
            TratarBotoes(instBtnAcionado);
            CarregarFormPanel(instBtnAcionado.Name);
        }
        private void btnImportarRendimentos_Click(object sender, EventArgs e)
        {
            Button instBtnAcionado = (Button)sender;
            TratarBotoes(instBtnAcionado);
            CarregarFormPanel(instBtnAcionado.Name);
        }
        #endregion

        #region MÉTODOS PIRVADOS
        private void CarregarFormPanel(string NomeBotao)
        {
            this.pnlConteudo.Controls.Clear();

            if (EnumBotoesLaterais.btnDashboard.ToString() == NomeBotao)
            {
                AdicionarFormConteudo(new frmDashboard());
            }
            else if (EnumBotoesLaterais.btnAnaliseDados.ToString() == NomeBotao)
            {
                AdicionarFormConteudo(new frmAnaliseDados());
            }
            else if (EnumBotoesLaterais.btnImportarDados.ToString() == NomeBotao)
            {
                AdicionarFormConteudo(new frmImportarDados());
            }
            else if (EnumBotoesLaterais.btnImportarOperacoes.ToString() == NomeBotao)
            {
                AdicionarFormConteudo(new frmImportarOperacoes());
            }
            else if (EnumBotoesLaterais.btnImportarRendimentos.ToString() == NomeBotao)
            {
                AdicionarFormConteudo(new frmImportarRendimentos());
            }
        }

        private void AdicionarFormConteudo(Form FormularioMenu)
        {
            FormularioMenu.TopLevel = false;
            FormularioMenu.AutoScroll = true;
            FormularioMenu.Dock = DockStyle.Fill;
            this.pnlConteudo.Controls.Add(FormularioMenu);
            FormularioMenu.Show();
        }

        private void ResetarBotoesLaterais()
        {
            foreach (EnumBotoesLaterais Botao in Enum.GetValues(typeof(EnumBotoesLaterais)))
            {
                Button btnLateral = (Button)pnlMenusLaterais.Controls.Find(Botao.ToString(), true)[0];
                btnLateral.BackColor = Color.FromArgb(45, 45, 73);
            }
        }

        private void TratarBotoes(Button BotaoAcionado)
        {
            this.ResetarBotoesLaterais();
            BotaoAcionado.BackColor = Color.FromArgb(68, 66, 109);
        }
        #endregion

        private void btnAjuda_Click(object sender, EventArgs e)
        {
            frmAjuda instFrmAjuda = new frmAjuda();
            instFrmAjuda.Show();
        }
    }
}
